/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_HELPER_H
#define DSV_HELPER_H

typedef struct _DSVBasicDrawingCommandsRequirements_t {
    DSVSwapchain    swapchain;
    VkRenderPass *  render_pass;
    VkPipeline *    graphic_pipeline;
    uint32_t        available_image_index;
} DSVBasicDrawingCommandsRequirements;

#endif
