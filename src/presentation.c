/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

VkResult DSV_queue_present(DSVSemaphores semaphores) {
    VkPresentInfoKHR present_info = {0};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.waitSemaphoreCount = semaphores.signals_count;
    present_info.pWaitSemaphores = semaphores.signals;
    
    //~ present_info.swapchainCount = 1;
    //~ present_info.pSwapchains = swapChains;
    //~ present_info.pImageIndices = &imageIndex;
    
    #ifdef DEBUG
    DSV_trace_VkPresentInfoKHR(&present_info);
    #endif
    
    return VK_SUCCESS;
}
