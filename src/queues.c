/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

void DSV_for_each_queue_familie(
    VkPhysicalDevice physical_device,
    int (*do_callback)(
        VkPhysicalDevice physical_device,
        uint32_t current_familiy_queue_index,
        VkQueueFamilyProperties * current_queue_family,
        void * do_callback_args
    ),
    void * do_callback_args
) {
    uint32_t queue_family_count = 0;
    VkQueueFamilyProperties * queue_families = 0;

    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, 0);
    queue_families = DSV_malloc(sizeof(VkQueueFamilyProperties) * queue_family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, queue_families);
    
    for (uint32_t i = 0; i < queue_family_count; i++) {
        if (do_callback(physical_device, i, queue_families, do_callback_args)) {
            break;
        }
    }
    
    DSV_free(queue_families);
}

// TODO: See vkGetDeviceQueue2 https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/vkGetDeviceQueue.html
VkQueue DSV_get_device_queue(
    VkDevice device,
    uint32_t queue_family_index,
    uint32_t queue_index,
    VkQueue* p_queue
) {
    VkQueue queue = VK_NULL_HANDLE;
    vkGetDeviceQueue(device, queue_family_index, queue_index, p_queue);
    if (queue == VK_NULL_HANDLE) {
        fprintf(stderr, "\e[1;31mDSV: failed to get queue.\n\e[0m");
    }
    return queue;
}
