/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "debug.h"
#include "memory_management.h"
#include "swap_chain.h"

uint32_t DSV_create_swap_chain(
    DSVDevice device,
    VkSurfaceKHR surface,
    uint32_t (*pick_swap_chain_surface_format_callback)(VkSurfaceFormatKHR * format, void * args),
    void * pick_swap_chain_surface_format_callback_args,
    VkSwapchainCreateInfoKHR * swap_chain_create_info,
    VkImageViewCreateInfo * image_view_create_info,
    DSVSwapChain swap_chain
) {
    VkSurfaceCapabilitiesKHR    capabilities = {0};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device->physical, surface, &capabilities);
    
    #ifdef DEBUG
    DSV_trace_VkSurfaceCapabilitiesKHR(&capabilities);
    #endif
    
    VkSurfaceFormatKHR surface_format = DSV_get_swap_surface_format(
        device->physical,
        surface,
        pick_swap_chain_surface_format_callback,
        pick_swap_chain_surface_format_callback_args
    );
    
    if(!DSV_swap_chain_extent_validation(capabilities)) {
        return 0;
    }
        
    if (capabilities.maxImageCount > 0 && swap_chain_create_info->minImageCount > capabilities.maxImageCount) {
        printf("DSV: Required swap chain image count greater than %u.\n", capabilities.maxImageCount);
        return 0;
    }
    
    swap_chain_create_info->imageFormat = surface_format.format;
    swap_chain_create_info->imageColorSpace = surface_format.colorSpace;
    swap_chain_create_info->imageExtent = capabilities.currentExtent;
    
    #ifdef DEBUG
    DSV_trace_VkSwapchainCreateInfoKHR(swap_chain_create_info);
    #endif
    
    if (vkCreateSwapchainKHR(device->logical, swap_chain_create_info, 0, &swap_chain->swap_chain) != VK_SUCCESS) {
        printf("DSV: Failed to create swap chain!\n");
        return 0;
    }
    swap_chain->extent = capabilities.currentExtent;
    vkGetSwapchainImagesKHR(device->logical, swap_chain->swap_chain, &swap_chain->count, NULL);
    swap_chain->images = DSV_malloc(sizeof(VkImage) * swap_chain->count);
    if (swap_chain->images == NULL) {
        printf("DSV: Failed to allocate swap chain images!\n");
        return 0;
    }
    if(vkGetSwapchainImagesKHR(device->logical, swap_chain->swap_chain, &swap_chain->count, swap_chain->images) != VK_SUCCESS) {
        printf("DSV: Failed to acquire images from swap chain!\n");
        DSV_free(swap_chain->images);
        return 0;
    };
    
    swap_chain->views = DSV_malloc(sizeof(VkImageView) * swap_chain->count);
    image_view_create_info->viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create_info->format = surface_format.format;
    
    for (int i = 0; i < swap_chain->count; i++) {
        image_view_create_info->image = swap_chain->images[i];

        printf("%u: ", i); DSV_trace_VkImageViewCreateInfo(image_view_create_info); 

        if (vkCreateImageView(device->logical, image_view_create_info, 0, &swap_chain->views[i]) != VK_SUCCESS) {
            printf("DSV: Failed to create image views!\n");
            DSV_free(swap_chain->views);
            return 0;
        }
    }

    return 1;
}

uint32_t DSV_swap_chain_extent_validation(
    const VkSurfaceCapabilitiesKHR capabilities
) {
    if (capabilities.currentExtent.width > capabilities.maxImageExtent.width) {
        printf("DSV: required swap chain image width is greater than %u.\n", capabilities.maxImageExtent.width);
        return 0;
    }
    else if (capabilities.currentExtent.width < capabilities.minImageExtent.width){
        printf("DSV: required swap chain image width is lesser than %u.\n", capabilities.minImageExtent.width);
        return 0;
    }
    
    if (capabilities.currentExtent.height > capabilities.maxImageExtent.height) {
        printf("DSV: required swap chain image height is greater than %u.\n", capabilities.maxImageExtent.height);
        return 0;
    }
    else if (capabilities.currentExtent.height < capabilities.minImageExtent.height){
        printf("DSV: required swap chain image height is lesser than %u.\n", capabilities.minImageExtent.height);
        return 0;
    }
    
    return 1;
}

VkExtent2D DSV_get_surface_current_extent(VkPhysicalDevice device, VkSurfaceKHR surface) {
    VkSurfaceCapabilitiesKHR    capabilities = {0};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &capabilities);
    return capabilities.currentExtent;
}

VkSurfaceFormatKHR DSV_get_swap_surface_format(
    VkPhysicalDevice physical_device,
    VkSurfaceKHR surface,
    uint32_t (*pick_swap_chain_surface_format_callback)(VkSurfaceFormatKHR * format, void * pick_swap_chain_surface_format_callback_args),
    void * pick_swap_chain_surface_format_callback_args
) {
  
    DSVDeviceSwapChainSupportDetails swap_chain_support_details = {0};
    swap_chain_support_details = DSV_get_device_swap_chain_support(
        surface,
        physical_device
    );
  
    for (int i=0; i < swap_chain_support_details.format_count; i++) {
        if (pick_swap_chain_surface_format_callback(
            &swap_chain_support_details.formats[i],
            pick_swap_chain_surface_format_callback_args
        )) {
            return swap_chain_support_details.formats[i];
        }
    }

    return swap_chain_support_details.formats[0];
}

VkSwapchainCreateInfoKHR DSV_init_VkSwapchainCreateInfoKHR(VkSurfaceKHR surface) {
    VkSwapchainCreateInfoKHR create_info = {0};
    create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    create_info.surface = surface;
    
    return create_info;
}
