/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DSV_MEMORY_MANAGEMENT_H
#define DSV_MEMORY_MANAGEMENT_H

#define DSV_ALLOCATION_REGISTER_CHUNK_SIZE 16

uint32_t DSV_init_memory_management();
uint32_t DSV_free(void * buffer);
void DSV_free_all();
void * DSV_malloc(uint32_t buffer_size);

#endif
