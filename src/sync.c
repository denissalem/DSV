/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "sync.h"

void DSV_free_semaphores(DSVDevice device, DSVSemaphores semaphores) {
  for (unsigned i = 0; i < semaphores.waits_count; i++) {
      vkDestroySemaphore(device.logical, semaphores.waits[i], NULL);
  }
  
  for (unsigned i = 0; i < semaphores.signals_count; i++) {
      vkDestroySemaphore(device.logical, semaphores.signals[i], NULL);
  }
  
  free(semaphores.waits);
  free(semaphores.wait_stages);
  free(semaphores.signals);
}

VkFence     DSV_get_fence(DSVDevice device, VkFenceCreateFlags flags) {
    VkFence fence = {0};
    VkFenceCreateInfo fence_create_info = {0};
    fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_create_info.flags = flags;
    VkResult result = vkCreateFence(device.logical, &fence_create_info, NULL, &fence);
    if ( result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkCreateFence failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
        return VK_NULL_HANDLE;
    }
    return fence;
}

// TODO : Register somehow allocated data for later cleaning

DSVSemaphores DSV_get_semaphores(DSVDevice device, unsigned int waits_count, unsigned int signals_count) {
    DSVSemaphores semaphores = {0};
    
    semaphores.waits_count = waits_count;
    semaphores.signals_count = signals_count;
    
    semaphores.waits = malloc(sizeof(VkSemaphore) * waits_count);
    semaphores.signals = malloc(sizeof(VkSemaphore) * signals_count);
    semaphores.wait_stages = malloc(sizeof(VkPipelineStageFlags) * waits_count);
    
    if (semaphores.waits == 0 || semaphores.wait_stages == 0 || semaphores.signals == 0) {
        fprintf(stderr, "\e[1;31mDSV: One or more attributes of DSVSemaphores have not been allocated.\e[0m\n");
        return semaphores;
    }
    
    VkSemaphoreCreateInfo semaphore_create_info = {0};
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    
    for (unsigned i = 0; i < waits_count; i++) {
        VkResult result = vkCreateSemaphore(device.logical, &semaphore_create_info, NULL, &semaphores.waits[i]);
        if (result != VK_SUCCESS) {
            fprintf(stderr, "\e[1;31mDSV: vkCreateSemaphore failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
            return semaphores;
        }
    }
    
    for (unsigned i = 0; i < signals_count; i++) {
        VkResult result = vkCreateSemaphore(device.logical, &semaphore_create_info, NULL, &semaphores.signals[i]);
        if (result != VK_SUCCESS) {
            fprintf(stderr, "\e[1;31mDSV: vkCreateSemaphore failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
            return semaphores;
        }
    }
    
    return semaphores;
}
