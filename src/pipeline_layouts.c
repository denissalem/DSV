/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "pipeline_layouts.h"

uint32_t DSV_create_pipeline_layout(
    VkDevice device,
    VkPipelineLayoutCreateInfo * create_info,
    VkPipelineLayout * layout
) {
    #ifdef DEBUG
    DSV_trace_VkPipelineLayoutCreateInfo(create_info);
    #endif
  
    VkResult result = vkCreatePipelineLayout(device, create_info, NULL, layout);
    if ( result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkCreatePipelineLayout failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
    }
    return result;
}
