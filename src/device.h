/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vulkan/vulkan.h>

#include "validation_layers.h"

#ifndef DSV_DEVICES_H
#define DSV_DEVICES_H

typedef struct DSVDevice_t {
    VkPhysicalDevice physical;
    VkDevice logical;
} DSVDevice;

typedef struct DSVPhysicalDevices_t {
    uint32_t count;
    VkPhysicalDevice * devices;
} DSVPhysicalDevices;

typedef struct DSVRequiredDeviceExtensions_t {
    uint32_t        count;
    const char *    names[];
} DSVRequiredDeviceExtensions;

VkDeviceCreateInfo DSV_allocate_VkDeviceCreateInfo(
    DSVRequiredValidationLayers * validation_layers, // Kept for retro compatibily
    DSVRequiredDeviceExtensions * extensions,
    uint32_t queue_families_count
);

VkResult DSV_pick_physical_device(
    VkInstance instance,
    DSVDevice * device,
    VkPhysicalDevice (*picker_callback)(DSVPhysicalDevices physicals, void * args),
    void * picked_callback_args
);

//~ typedef struct DSVDeviceSwapChainSupportDetails_t {
    //~ VkSurfaceCapabilitiesKHR    capabilities;
    //~ VkSurfaceFormatKHR *        formats;
    //~ VkPresentModeKHR *          present_modes;
    //~ uint32_t format_count;
    //~ uint32_t present_mode_count;
//~ } DSVDeviceSwapChainSupportDetails;



//~ uint32_t DSV_check_device_extension_support(
    //~ VkPhysicalDevice device,
    //~ VkDeviceCreateInfo * device_create_info
//~ );

//~ void DSV_clear_device_swap_chain_support(DSVDeviceSwapChainSupportDetails * swap_chain_support_details);

//~ void DSV_clear_VkDeviceCreateInfo(VkDeviceCreateInfo * create_info);

//~ uint32_t DSV_create_logical_device(
  //~ VkDeviceCreateInfo * create_info,
  //~ VkPhysicalDevice physical,
  //~ VkDevice * logical
//~ );

//~ uint32_t DSV_get_device(
    //~ uint32_t (*match_physical_device_n_write_create_info_callback)(
        //~ VkInstance * instance,
        //~ VkSurfaceKHR * surface,
        //~ VkPhysicalDevice physical_device,
        //~ VkDeviceCreateInfo * create_info,
        //~ void * args
    //~ ),
    //~ VkInstance * instance,
    //~ VkSurfaceKHR * surface,
    //~ VkDeviceCreateInfo * create_info,
    //~ void * match_physical_device_n_write_create_info_callback_args,
    //~ DSVDevice * device
//~ );

//~ DSVDeviceSwapChainSupportDetails DSV_get_device_swap_chain_support(
  //~ VkSurfaceKHR surface,
  //~ VkPhysicalDevice physical_device
//~ );

//~ VkDeviceCreateInfo DSV_get_VkDeviceCreateInfo(
    //~ DSVRequiredValidationLayers * validation_layers, // Kept for retro compatibily
    //~ DSVRequiredDeviceExtensions * extensions,
    //~ uint32_t queue_create_info_count
//~ );

#endif
