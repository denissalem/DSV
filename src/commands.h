/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_COMMANDS_H
#define DSV_COMMANDS_H

#include "sync.h"

uint32_t DSV_create_command_pool(
    VkDevice device,
    VkCommandPoolCreateFlags flags,
    uint32_t queue_family_index,
    VkCommandPool * command_pool
);

uint32_t DSV_create_command_buffer(
    VkDevice device,
    VkCommandPool * command_pool,
    VkCommandBuffer * command_buffer,
    VkCommandBufferLevel level,
    uint32_t command_buffer_count
);

uint32_t DSV_record_commands(
    VkCommandBuffer * commandBuffer,
    VkCommandBufferUsageFlags flags, 
    const VkCommandBufferInheritanceInfo * pInheritanceInfo,
    uint32_t (*commands_callback)(VkCommandBuffer * command_buffer, void * commands_callback_args),
    void * commands_callback_args
);

void DSV_set_scissor_and_viewport(
    VkCommandBuffer * command_buffer,
    VkExtent2D extent
);

uint32_t DSV_submit_commands(
    VkCommandBuffer * command_buffer,
    unsigned int buffer_count,
    DSVSemaphores semaphores,
    VkFence fence,
    VkQueue queue
);

#endif
