/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

uint32_t DSV_create_command_pool(
    VkDevice device,
    VkCommandPoolCreateFlags flags,
    uint32_t queue_family_index,
    VkCommandPool * command_pool
) {
    VkCommandPoolCreateInfo create_info = {0};
    create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    create_info.flags = flags;
    create_info.queueFamilyIndex = queue_family_index;
    #ifdef DEBUG
    DSV_trace_VkCommandPoolCreateInfo(&create_info);
    #endif
    
    VkResult result = vkCreateCommandPool(device, &create_info, NULL, command_pool);
    if ( result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkCreateCommandPool failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
    }
    
    return result;
}

uint32_t DSV_create_command_buffer(
    VkDevice device,
    VkCommandPool * command_pool,
    VkCommandBuffer * command_buffer,
    VkCommandBufferLevel level,
    uint32_t command_buffer_count
) {
    VkCommandBufferAllocateInfo alloc_info = {0};
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.commandPool = *command_pool;
    alloc_info.level = level;
    alloc_info.commandBufferCount = command_buffer_count;
    #ifdef DEBUG
    DSV_trace_VkCommandBufferAllocateInfo(&alloc_info);
    #endif
    
    VkResult result = vkAllocateCommandBuffers(device, &alloc_info, command_buffer);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkAllocateCommandBuffers failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
    }
    
    return result;
}

uint32_t DSV_record_commands(
    VkCommandBuffer * command_buffer,
    VkCommandBufferUsageFlags flags, 
    const VkCommandBufferInheritanceInfo * pInheritanceInfo,
    uint32_t (*commands_callback)(VkCommandBuffer * command_buffer, void * commands_callback_args),
    void * commands_callback_args
) {
    VkResult result = vkResetCommandBuffer(*command_buffer, 0);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkResetCommandBuffer failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
        return result;
    }
    
    VkCommandBufferBeginInfo begin_info = {0};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags = flags;
    begin_info.pInheritanceInfo = pInheritanceInfo;

    #ifdef DEBUG
    DSV_trace_VkCommandBufferBeginInfo(&begin_info);
    #endif
    
    result = vkBeginCommandBuffer(*command_buffer, &begin_info);

    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkBeginCommandBuffer failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
        return result;
    }

    result = commands_callback(command_buffer, commands_callback_args);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: Client defined commands callback failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
        return result;
    }
        
    result = vkEndCommandBuffer(*command_buffer);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkEndCommandBuffer failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
    }
    
    return result;
}

void DSV_set_scissor_and_viewport(VkCommandBuffer * command_buffer, VkExtent2D extent) {
    VkViewport viewport = {0};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) extent.width;
    viewport.height = (float) extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport(*command_buffer, 0, 1, &viewport);
    
    VkRect2D scissor = {0};
    scissor.extent = extent;
    vkCmdSetScissor(*command_buffer, 0, 1, &scissor);
}


// TODO: In its actual form, the following code is not efficient :
// "The function vkQueueSubmit takes an array of VkSubmitInfo structures as argument for efficiency when the workload is much larger."
uint32_t DSV_submit_commands(
    VkCommandBuffer * command_buffer,
    unsigned int buffer_count,
    DSVSemaphores semaphores,
    VkFence fence,
    VkQueue queue
) {
    VkSubmitInfo submit_info = {0};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.waitSemaphoreCount = semaphores.waits_count;
    submit_info.pWaitSemaphores = semaphores.waits;
    submit_info.pWaitDstStageMask = semaphores.wait_stages;
    submit_info.commandBufferCount = buffer_count;
    submit_info.pCommandBuffers = command_buffer;
    submit_info.signalSemaphoreCount = semaphores.signals_count;
    submit_info.pSignalSemaphores = semaphores.signals;

    #ifdef DEBUG
    DSV_trace_VkSubmitInfo(&submit_info);
    #endif
    
    VkResult result = vkQueueSubmit(queue, 1, &submit_info, fence);
    if ( result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: failed to submit draw command buffer.\n\e[0m");
        return result;
    }
    
    return result;
}
