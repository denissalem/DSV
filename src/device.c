/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdio.h"
#include "string.h"

#include "debug.h"
#include "device.h"
#include "memory_management.h"

VkDeviceCreateInfo DSV_allocate_VkDeviceCreateInfo(
    DSVRequiredValidationLayers * validation_layers, // Kept for retro compatibily
    DSVRequiredDeviceExtensions * extensions,
    uint32_t queue_families_count
) {
    VkDeviceCreateInfo create_info = {0};
    create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    
    create_info.pEnabledFeatures = DSV_malloc(sizeof(VkPhysicalDeviceFeatures));

    if (extensions != NULL) {
        create_info.enabledExtensionCount = extensions->count;
        create_info.ppEnabledExtensionNames = extensions->names;
    }
    
    if (validation_layers != NULL) {
        create_info.enabledLayerCount = validation_layers->count;
        create_info.ppEnabledLayerNames = validation_layers->names;
    }

    create_info.queueCreateInfoCount = queue_families_count;

    return create_info;
}

VkResult DSV_pick_physical_device(
    VkInstance instance,
    DSVDevice * device,
    VkPhysicalDevice (*picker_callback)(DSVPhysicalDevices physicals, void * args),
    void * picked_callback_args
) {
    DSVPhysicalDevices physicals;
    VkResult result;

    result = vkEnumeratePhysicalDevices(instance, &physicals.count, 0);
    DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(vkEnumeratePhysicalDevices, result);
    
    if (physicals.count == 0) {
        DSV_TRACE_ERROR("No GPU Available.");
        return result;
    }
    
    physicals.devices = DSV_malloc(sizeof(VkPhysicalDevice) * physicals.count);
    
    result = vkEnumeratePhysicalDevices(instance, &physicals.count, physicals.devices);
    DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(vkEnumeratePhysicalDevices, result);
    
    device->physical = picker_callback(physicals, picked_callback_args);
    if (device->physical == VK_NULL_HANDLE) {
        return VK_ERROR_UNKNOWN;
    }
}

//~ uint32_t DSV_check_device_extension_support(
    //~ VkPhysicalDevice physical_device,
    //~ VkDeviceCreateInfo * device_create_info
//~ ) {
    //~ uint32_t available_extension_count;
    //~ vkEnumerateDeviceExtensionProperties(physical_device, 0, &available_extension_count, 0);

    //~ VkExtensionProperties * available_extensions = DSV_malloc(sizeof(VkExtensionProperties) * available_extension_count);
    //~ vkEnumerateDeviceExtensionProperties(physical_device, 0, &available_extension_count, available_extensions);

    //~ for (int i = 0; i < device_create_info->enabledExtensionCount; i++) {
        //~ int extension_found = 0;
        //~ for (int j = 0; j < available_extension_count; j++) {
            //~ if (strcmp(device_create_info->ppEnabledExtensionNames[i], available_extensions[j].extensionName) == 0) {
                //~ extension_found = 1;
                //~ break;
            //~ }
        //~ }
        //~ if (!extension_found) {
            //~ printf("DSV: Missing %s\n", device_create_info->ppEnabledExtensionNames[i]);
            //~ DSV_free(available_extensions);
            //~ return 0;
        //~ }
    //~ }
    //~ DSV_free(available_extensions);
    //~ return 1;
//~ }

//~ void DSV_clear_device_swap_chain_support(DSVDeviceSwapChainSupportDetails * swap_chain_support_details) {
    //~ if (swap_chain_support_details->formats) {
        //~ DSV_free(swap_chain_support_details->formats);
        //~ swap_chain_support_details->formats = NULL;
    //~ }
    //~ if (swap_chain_support_details->present_modes) {
        //~ DSV_free(swap_chain_support_details->present_modes);
        //~ swap_chain_support_details->present_modes = NULL;
    //~ }
//~ };

//~ void DSV_clear_VkDeviceCreateInfo(VkDeviceCreateInfo * create_info) {
    //~ DSV_free((void*)create_info->pEnabledFeatures);
    //~ create_info->pEnabledFeatures = 0;
    //~ DSV_free((void*)create_info->pQueueCreateInfos);
    //~ create_info->pQueueCreateInfos = 0;
//~ }

//~ uint32_t DSV_create_logical_device(
  //~ VkDeviceCreateInfo * create_info,
  //~ VkPhysicalDevice physical,
  //~ VkDevice * logical
//~ ) {
    //~ #ifdef DEBUG
    //~ VkPhysicalDeviceProperties properties = {0};
    //~ vkGetPhysicalDeviceProperties(physical, &properties);
    //~ printf("DSV: Selected device: %s.\n", properties.deviceName);
    //~ DSV_trace_VkDeviceCreateInfo(create_info);
    //~ #endif

    //~ VkResult result = vkCreateDevice(physical, create_info, 0, logical);
    //~ if (result != VK_SUCCESS) {
        //~ printf("DSV: Failed to create logical device with VkResult = %d!\n", result);
        //~ return 0;
    //~ }
    //~ return 1;
//~ } 

//~ uint32_t DSV_get_device(
    //~ uint32_t (*match_physical_device_n_write_create_info_callback)(
        //~ VkInstance * instance,
        //~ DSVSurface * surface,
        //~ VkPhysicalDevice physical_device,
        //~ VkDeviceCreateInfo * create_info,
        //~ void * args
    //~ ),
    //~ VkInstance * instance,
    //~ DSVSurface * surface,
    //~ VkDeviceCreateInfo * create_info,
    //~ void * match_physical_device_n_write_create_info_callback_args,
    //~ DSVDevice * device
//~ ) {

    //~ uint32_t logical_device_created = 0;
    //~ for (uint32_t i = 0; i < physical_devices_count; i++) {
        //~ if(
            //~ match_physical_device_n_write_create_info_callback(
                //~ instance,
                //~ surface,
                //~ physical_devices[i], 
                //~ create_info,
                //~ match_physical_device_n_write_create_info_callback_args
            //~ )
        //~ ){
            //~ logical_device_created = DSV_create_logical_device(
                //~ create_info,
                //~ physical_devices[i],
                //~ &device->logical
            //~ );
            //~ if (logical_device_created) {
                //~ device->physical = physical_devices[i];
                //~ break;
            //~ }
        //~ }
    //~ }

    //~ DSV_free(physical_devices);

    //~ if (!logical_device_created) {
        //~ printf("DSV: Failed to find a suitable GPU!\n");
        //~ return 0;
    //~ }
    
    //~ return 1;
//~ }

//~ DSVDeviceSwapChainSupportDetails DSV_get_device_swap_chain_support(
  //~ VkSurfaceKHR surface,
  //~ VkPhysicalDevice physical_device
//~ ) {
    //~ DSVDeviceSwapChainSupportDetails details = {0};
    
    //~ vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &details.capabilities);
    //~ vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &details.format_count, 0);
    
    //~ if (details.format_count != 0) {
        //~ details.formats = DSV_malloc(sizeof(VkSurfaceFormatKHR) * details.format_count);
        //~ vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &details.format_count, details.formats);
    //~ }
    
    //~ vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &details.present_mode_count, 0);

    //~ if (details.present_mode_count != 0) {
        //~ details.present_modes = DSV_malloc(sizeof(VkPresentModeKHR) * details.present_mode_count);
        //~ vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &details.present_mode_count, details.present_modes);
    //~ }

    //~ return details;
//~ }
