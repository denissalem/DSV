/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

VkResult DSV_create_instance(
    VkInstance * instance,
    DSVRequiredInstanceExtensions * required_extensions,
    DSVRequiredValidationLayers * required_validation_layers,
    const char * application_name,
    uint32_t application_version,
    const char * engine_name,
    uint32_t engine_version,
    uint32_t vulkan_api_version
) {
    VkInstanceCreateInfo instance_create_info = {0};
    VkApplicationInfo application_info = {0};
    
    application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    application_info.pApplicationName = application_name;
    application_info.applicationVersion = application_version;
    application_info.pEngineName = engine_name;
    application_info.engineVersion = engine_version;
    application_info.apiVersion = vulkan_api_version;
    
    instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_create_info.pApplicationInfo = &application_info;
    instance_create_info.enabledExtensionCount = required_extensions->count;
    instance_create_info.ppEnabledExtensionNames = required_extensions->names;
    
    VkResult result = DSV_check_validation_layer_support(required_validation_layers);
    DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(vkCreateInstance, result);
    
    instance_create_info.enabledLayerCount = required_validation_layers->count;
    instance_create_info.ppEnabledLayerNames = required_validation_layers->names;
    
    #ifdef DEBUG
    uint32_t available_extensions_count = 0;
    VkExtensionProperties * available_extensions = 0;
    vkEnumerateInstanceExtensionProperties(0, &available_extensions_count, 0);
    available_extensions = DSV_malloc(sizeof(VkExtensionProperties)*available_extensions_count);
    vkEnumerateInstanceExtensionProperties(0, &available_extensions_count, available_extensions);
    printf("DSV: Available instance extensions:\n");
    for (int i=0; i<available_extensions_count; i++) {
        printf("\t%s\n", available_extensions[i].extensionName);
    }
    DSV_free(available_extensions);
    #endif
    
    result = vkCreateInstance(&instance_create_info, 0, instance);
    DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(vkCreateInstance, result);

    return result;
}
