/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef DEBUG

#include <stdio.h>
#include  <string.h>

#include "../debug.h"

void DSV_trace_VkCommandBufferAllocateInfo(VkCommandBufferAllocateInfo * alloc_info) {
    printf("DSV: VkCommandBufferAllocateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(alloc_info->sType, " ");
    printf("\tpNext: %lX\n", (unsigned long) alloc_info->pNext);
    printf("\tcommandPool: %lX\n", (unsigned long) alloc_info->commandPool);
    printf("\tlevel:"); DSV_print_enum_VkCommandBufferLevel(alloc_info->level, " ");
    printf("\tcommandBufferCount: %lu\n", (unsigned long) alloc_info->commandBufferCount);
}

void DSV_trace_VkCommandBufferBeginInfo(VkCommandBufferBeginInfo * begin_info) {
    printf("DSV: VkCommandBufferBeginInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(begin_info->sType, " ");
    printf("\tflags:\n"); DSV_print_flag_VkCommandBufferUsageFlag(begin_info->flags, "\t\t");
    printf("\tpInheritanceInfo: %s\n", begin_info->pInheritanceInfo != NULL ? "" : "Not Set");
    if (begin_info->pInheritanceInfo != NULL) {
        printf("\t\tsType:"); DSV_print_enum_VkStructureType(begin_info->pInheritanceInfo->sType, " ");
        printf("\t\tpNext: %lu\n", (unsigned long) begin_info->pInheritanceInfo->pNext);
        printf("\t\trenderPass: %lX\n", (long unsigned) begin_info->pInheritanceInfo->renderPass);
        printf("\t\tsubpass: %u\n", begin_info->pInheritanceInfo->subpass);
        printf("\tframebuffer: %lX\n", (long unsigned) begin_info->pInheritanceInfo->framebuffer);
        printf("\tocclusionQueryEnable:"); DSV_print_enum_VkBool32(begin_info->pInheritanceInfo->occlusionQueryEnable, " ");
        printf("\tqueryFlags: "); DSV_print_flag_VkQueryControlFlags(begin_info->pInheritanceInfo->queryFlags, "");
        printf("\tpipelineStatistics: "); DSV_print_flag_VkQueryPipelineStatisticFlags(begin_info->pInheritanceInfo->pipelineStatistics, "");
    }
}

void DSV_trace_VkPipelineColorBlendAttachmentState(VkPipelineColorBlendAttachmentState * attachments) {
    _indent_DSV_trace_VkPipelineColorBlendAttachmentState(attachments, "");
}

void DSV_trace_VkPresentInfoKHR(VkPresentInfoKHR * info) {
    printf("DSV: VkPresentInfoKHR has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(info->sType, " ");
    printf("\tpNext: 0x%lX\n", (unsigned long) info->pNext);
    printf("\twaitSemaphoreCount: %u\n", info->waitSemaphoreCount);
    printf("\tpWaitSemaphores:\n");
    if (info->waitSemaphoreCount > 0) {
        if (info->pWaitSemaphores == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pWaitSemaphores is NULL but waitSemaphoreCount is %u\n\e[0m\n", info->waitSemaphoreCount);
        }
        else {
            for (unsigned int i = 0; i < info->waitSemaphoreCount; i++) {
                if (info->pWaitSemaphores[i] == VK_NULL_HANDLE) {
                    fprintf(stderr, "\e[1;31m\t\tNULL VkSemaphore at index %u.\n\e[0m\n", i);
                }
                else {
                    printf("\t\t0x%lX\n", (unsigned long) info->pWaitSemaphores[i]);
                }
            }
        }
    }
    printf("\tswapchainCount: %u\n", info->swapchainCount);
    printf("\tpSwapchains:\n");
    if (info->swapchainCount > 0) {
        if (info->pSwapchains == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pSwapchains is NULL but swapchainCount is %u\n\e[0m\n", info->swapchainCount);
        }
        else {
            for (unsigned int i = 0; i < info->swapchainCount; i++) {
                if (info->pSwapchains[i] == VK_NULL_HANDLE) {
                    fprintf(stderr, "\e[1;31m\t\tNULL VkSwapchainKHR at index %u.\n\e[0m\n", i);
                }
                else {
                    printf("\t\t0x%lX\n", (unsigned long) info->pSwapchains[i]);
                }
            }
        }
    }
    printf("\tpImageIndices:\n");
    if (info->swapchainCount > 0) {
        if (info->pImageIndices == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pImageIndices is NULL but swapchainCount is %u\n\e[0m\n", info->swapchainCount);
        }
        else {
            for (unsigned int i = 0; i < info->swapchainCount; i++) {
                printf("\t\t%u\n",  info->pImageIndices[i]);
            }
        }
    }
    if (info->swapchainCount > 0) {
        if (info->pImageIndices == NULL) {
            printf("\tpResults: NULL\n");
        }
        else {
            printf("\tpResults:\n");
            for (unsigned int i = 0; i < info->swapchainCount; i++) {
                DSV_print_enum_VkResult(info->pResults[i], "\t\t");
            }
        }
    }
}

void DSV_trace_VkRenderPassBeginInfo(VkRenderPassBeginInfo * info) {
    printf("DSV: VkRenderPassBeginInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(info->sType, " ");
    printf("\tpNext: 0x%lX\n", (unsigned long) info->pNext);
    printf("\trenderPass: %lX\n", (long unsigned) info->renderPass);
    printf("\tframebuffer: %lX\n", (long unsigned) info->framebuffer);
    printf("\trenderArea:\n");
    printf("\t\toffset:\n");
    printf("\t\t\tx: %d\n", info->renderArea.offset.x);
    printf("\t\t\ty: %d\n", info->renderArea.offset.y);
    printf("\t\textent:\n");
    printf("\t\t\twidth: %d\n", info->renderArea.extent.width);
    printf("\t\t\theight: %d\n", info->renderArea.extent.height);
    printf("\tclearValueCount: %u\n", info->clearValueCount);
    printf("\tpClearValues:\n");
    // TODO Check for NULL pointer / Value
    for (unsigned i = 0; i < info->clearValueCount; i++) {
        printf("\t\tcolor:\n");
            printf(
                "\t\t\tfloat32: %f %f %f %f\n",
                info->pClearValues[i].color.float32[0],
                info->pClearValues[i].color.float32[1],
                info->pClearValues[i].color.float32[2],
                info->pClearValues[i].color.float32[3]
            );
            printf(
                "\t\t\tint32: %d %d %d %d\n",
                info->pClearValues[i].color.int32[0],
                info->pClearValues[i].color.int32[1],
                info->pClearValues[i].color.int32[2],
                info->pClearValues[i].color.int32[3]
            );
            printf(
                "\t\t\tuint32: %u %u %u %u\n",
                info->pClearValues[i].color.uint32[0],
                info->pClearValues[i].color.uint32[1],
                info->pClearValues[i].color.uint32[2],
                info->pClearValues[i].color.uint32[3]
            );            
        printf("\t\tdepthStencil:\n");
            printf("\t\t\tdepth: %f\n", info->pClearValues[i].depthStencil.depth);
            printf("\t\t\tstencil: %u\n", info->pClearValues[i].depthStencil.stencil);
    }
}

void DSV_trace_VkSurfaceCapabilitiesKHR(VkSurfaceCapabilitiesKHR * capabilities) {
    printf("DSV: VkSurfaceCapabilitisKHR has the following values:\n");
    printf("\tminImageCount: %u\n", capabilities->minImageCount);
    printf("\tmaxImageCount: %u %s\n", capabilities->maxImageCount, capabilities->maxImageCount == 0 ? "(unlimited until memory exhaustion)" : "");
    printf("\tcurrentExtent:\n");
    printf("\t\twidth: %u\n", capabilities->currentExtent.width);
    printf("\t\theight: %u\n", capabilities->currentExtent.height);
    printf("\tminImageExtent:\n");
    printf("\t\twidth: %u\n", capabilities->minImageExtent.width);
    printf("\t\theight: %u\n", capabilities->minImageExtent.height);
    printf("\tmaxImageExtent:\n");
    printf("\t\twidth: %u\n", capabilities->maxImageExtent.width);
    printf("\t\theight: %u\n", capabilities->maxImageExtent.height);
    printf("\tsupportedTransforms:\n"); DSV_print_flag_VkSurfaceTransformFlagsKHR(capabilities->supportedTransforms, "\t\t");
    printf("\tcurrentTransform:\n"); DSV_print_flag_VkSurfaceTransformFlagsKHR(capabilities->currentTransform, "\t\t");
    printf("\tsupportedCompositeAlpha:\n"); DSV_print_flag_VkCompositeAlphaFlagsKHR(capabilities->supportedCompositeAlpha, "\t\t");
    printf("\tsupportedUsageFlags:\n"); DSV_print_flag_VkImageUsageFlags(capabilities->supportedUsageFlags, "\t\t");
}

void DSV_trace_VkStencilOpState(VkStencilOpState * stencil_op_state) {
    _indent_DSV_trace_VkStencilOpState(stencil_op_state, "");
}

void DSV_trace_VkSubmitInfo(VkSubmitInfo * info) {
    printf("DSV: VkSubmitInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) info->pNext);
    printf("\twaitSemaphoreCount: %u\n", info->waitSemaphoreCount);
    printf("\tpWaitSemaphores:\n");
    if (info->waitSemaphoreCount > 0) {
        if (info->pWaitSemaphores == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pWaitSemaphores is NULL but waitSemaphoreCount is %u\n\e[0m\n", info->waitSemaphoreCount);
        }
        else {
            for (unsigned int i = 0; i < info->waitSemaphoreCount; i++) {
                if (info->pWaitSemaphores[i] == VK_NULL_HANDLE) {
                    fprintf(stderr, "\e[1;31m\t\tNULL VkSemaphore at index %u.\n\e[0m\n", i);
                }
                else {
                    printf("\t\t0x%lX\n", (unsigned long) info->pWaitSemaphores[i]);
                }
            }
        }
    }
    printf("\tpWaitDstStageMask:\n");
    if (info->waitSemaphoreCount > 0) {
        if (info->pWaitDstStageMask == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pWaitDstStageMask is NULL but waitSemaphoreCount is %u\n\e[0m\n", info->waitSemaphoreCount);
        }
        else {
            for (unsigned int i = 0; i < info->waitSemaphoreCount; i++) {
                printf("\t\t%u:\n", i); DSV_print_flag_VkPipelineStageFlags(info->pWaitDstStageMask[i], "\t\t\t");
            }
        }
    }
    printf("\tcommandBufferCount: %u\n", info->commandBufferCount);
    printf("\tpCommandBuffers:\n");
    if (info->commandBufferCount > 0) {
        if (info->pCommandBuffers == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pCommandBuffers is NULL but commandBufferCount is %u\n\e[0m\n", info->commandBufferCount);
        }
        else {
            for (unsigned int i = 0; i < info->commandBufferCount; i++) {
                if (info->pCommandBuffers[i] == VK_NULL_HANDLE) {
                    fprintf(stderr, "\e[1;31m\t\tNULL VkCommandBuffer at index %u.\n\e[0m\n", i);
                }
                else {
                    printf("\t\t0x%lX\n", (unsigned long) info->pCommandBuffers[i]);
                }
            }
        }
    }
    printf("\tsignalSemaphoreCount: %u\n", info->signalSemaphoreCount);
    printf("\tpSignalSemaphores:\n");
    if (info->signalSemaphoreCount > 0) {
        if (info->pSignalSemaphores == NULL) {
            fprintf(stderr, "\e[1;31mDSV: pSignalSemaphores is NULL but signalSemaphoreCount is %u\n\e[0m\n", info->signalSemaphoreCount);
        }
        else {
            for (unsigned int i = 0; i < info->signalSemaphoreCount; i++) {
                if (info->pSignalSemaphores[i] == VK_NULL_HANDLE) {
                    fprintf(stderr, "\e[1;31m\t\tNULL VkSemaphore at index %u.\n\e[0m\n", i);
                }
                else {
                    printf("\t\t0x%lX\n", (unsigned long) info->pSignalSemaphores[i]);
                }
            }
        }
    }
}

void _indent_DSV_trace_VkPipelineColorBlendAttachmentState(VkPipelineColorBlendAttachmentState * attachments, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineColorBlendAttachmentState has the following values:\n");
    }
    printf("%s\tblendEnable:", indent); DSV_print_enum_VkBool32(attachments->blendEnable, " ");
    printf("%s\tsrcColorBlendFactor:", indent); DSV_print_enum_VkBlendFactor(attachments->srcColorBlendFactor, " ");
    printf("%s\tdstColorBlendFactor:", indent); DSV_print_enum_VkBlendFactor(attachments->dstColorBlendFactor, " ");
    printf("%s\tcolorBlendOp:", indent); DSV_print_enum_VkBlendOp(attachments->colorBlendOp, " ");
    printf("%s\tsrcAlphaBlendFactor:", indent); DSV_print_enum_VkBlendFactor(attachments->srcAlphaBlendFactor, " ");
    printf("%s\tdstAlphaBlendFactor:", indent); DSV_print_enum_VkBlendFactor(attachments->dstAlphaBlendFactor, " ");
    printf("%s\talphaBlendOp:", indent); DSV_print_enum_VkBlendOp(attachments->alphaBlendOp, " ");
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tcolorWriteMask:\n", indent); DSV_print_flag_VkColorComponentFlags(attachments->colorWriteMask, indent_extra);
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkStencilOpState(VkStencilOpState * stencil_op_state, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkStencilOpState has the following values:\n");
    }
    printf("%s\tfailOp:", indent); DSV_print_enum_VkStencilOp(stencil_op_state->failOp, " ");
    printf("%s\tpassOp:", indent); DSV_print_enum_VkCompareOp(stencil_op_state->passOp, " ");
    printf("%s\tdepthFailOp:", indent); DSV_print_enum_VkCompareOp(stencil_op_state->depthFailOp, " ");
    printf("%s\tcompareOp:", indent); DSV_print_enum_VkCompareOp(stencil_op_state->compareOp, " ");
    printf("%s\tcompareMask: %u\n", indent, stencil_op_state->compareMask);
    printf("%s\twriteMask: %u\n", indent, stencil_op_state->writeMask);
    printf("%s\treference: %u\n", indent, stencil_op_state->reference);
}

#endif
