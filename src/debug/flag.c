/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/


#ifdef DEBUG

#include <stdio.h>

#include "../debug.h"

void DSV_print_flag_VkAccessFlags(uint32_t flag, const char * indent_level) {
    #if defined(VK_VERSION_1_3)
    if flag == VK_ACCESS_NONE:
        printf("%sVK_ACCESS_NONE\n", indent_level);
        return;
    #endif
    
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_ACCESS_INDIRECT_COMMAND_READ_BIT:
                printf("%sVK_ACCESS_INDIRECT_COMMAND_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_INDEX_READ_BIT:
                printf("%sVK_ACCESS_INDEX_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT:
                printf("%sVK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_UNIFORM_READ_BIT:
                printf("%sVK_ACCESS_UNIFORM_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_INPUT_ATTACHMENT_READ_BIT:
                printf("%sVK_ACCESS_INPUT_ATTACHMENT_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_SHADER_READ_BIT:
                printf("%sVK_ACCESS_SHADER_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_SHADER_WRITE_BIT:
                printf("%sVK_ACCESS_SHADER_WRITE_BIT\n", indent_level);
                break;
            case VK_ACCESS_COLOR_ATTACHMENT_READ_BIT:
                printf("%sVK_ACCESS_COLOR_ATTACHMENT_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT:
                printf("%sVK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT\n", indent_level);
                break;
            case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT:
                printf("%sVK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT:
                printf("%sVK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT\n", indent_level);
                break;
            case VK_ACCESS_TRANSFER_READ_BIT:
                printf("%sVK_ACCESS_TRANSFER_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_TRANSFER_WRITE_BIT:
                printf("%sVK_ACCESS_TRANSFER_WRITE_BIT\n", indent_level);
                break;
            case VK_ACCESS_HOST_READ_BIT:
                printf("%sVK_ACCESS_HOST_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_HOST_WRITE_BIT:
                printf("%sVK_ACCESS_HOST_WRITE_BIT\n", indent_level);
                break;
            case VK_ACCESS_MEMORY_READ_BIT:
                printf("%sVK_ACCESS_MEMORY_READ_BIT\n", indent_level);
                break;
            case VK_ACCESS_MEMORY_WRITE_BIT:
                printf("%sVK_ACCESS_MEMORY_WRITE_BIT\n", indent_level);
                break;
        #if defined(VK_EXT_transform_feedback)
            case VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT:
                printf("%sVK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_transform_feedback)
            case VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT:
                printf("%sVK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_transform_feedback)
            case VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT:
                printf("%sVK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_conditional_rendering)
            case VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT:
                printf("%sVK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_blend_operation_advanced)
            case VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT:
                printf("%sVK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_acceleration_structure)
            case VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR:
                printf("%sVK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_acceleration_structure)
            case VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR:
                printf("%sVK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_fragment_density_map)
            case VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT:
                printf("%sVK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_fragment_shading_rate)
            case VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR:
                printf("%sVK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_device_generated_commands)
            case VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV:
                printf("%sVK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_device_generated_commands)
            case VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV:
                printf("%sVK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_shading_rate_image)
            case VK_ACCESS_SHADING_RATE_IMAGE_READ_BIT_NV:
                printf("%sVK_ACCESS_SHADING_RATE_IMAGE_READ_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV:
                printf("%sVK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV:
                printf("%sVK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_synchronization2)
            case VK_ACCESS_NONE_KHR:
                printf("%sVK_ACCESS_NONE_KHR\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkAccessFlag: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkAttachmentDescriptionFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT:
                printf("%sVK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkAttachmentDescriptionFlag: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkColorComponentFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_COLOR_COMPONENT_R_BIT:
                printf("%sVK_COLOR_COMPONENT_R_BIT\n", indent_level);
                break;
            case VK_COLOR_COMPONENT_G_BIT:
                printf("%sVK_COLOR_COMPONENT_G_BIT\n", indent_level);
                break;
            case VK_COLOR_COMPONENT_B_BIT:
                printf("%sVK_COLOR_COMPONENT_B_BIT\n", indent_level);
                break;
            case VK_COLOR_COMPONENT_A_BIT:
                printf("%sVK_COLOR_COMPONENT_A_BIT\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkColorComponentFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkCommandBufferUsageFlag(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT:
                printf("%sVK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT\n", indent_level);
                break;
            case VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT:
                printf("%sVK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT\n", indent_level);
                break;
            case VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT:
                printf("%sVK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkColorComponentFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkCommandPoolCreateFlag(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_COMMAND_POOL_CREATE_TRANSIENT_BIT:
                printf("%sVK_COMMAND_POOL_CREATE_TRANSIENT_BIT\n", indent_level);
                break;
            case VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT:
                printf("%sVK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT\n", indent_level);
                break;
            case VK_COMMAND_POOL_CREATE_PROTECTED_BIT:
                printf("%sVK_COMMAND_POOL_CREATE_PROTECTED_BIT\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkColorComponentFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void  DSV_print_flag_VkCompositeAlphaFlagsKHR(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR:
                printf("%sVK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR\n", indent_level);
                break;
            case VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR:
                printf("%sVK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR\n", indent_level);
                break;
            case VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR:
                printf("%sVK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR\n", indent_level);
                break;
            case VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR:
                printf("%sVK_COMPOSITE_ALPHA_INHERIT_BIT_KHR\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkCompositeAlphaFlagBitsKHR: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkDependencyFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            #ifdef VK_VERSION_1_1
            case VK_DEPENDENCY_BY_REGION_BIT:
                printf("%sVK_DEPENDENCY_BY_REGION_BIT\n", indent_level);
                break;
            case VK_DEPENDENCY_DEVICE_GROUP_BIT:
                printf("%sVK_DEPENDENCY_DEVICE_GROUP_BIT\n", indent_level);
                break;
            case VK_DEPENDENCY_VIEW_LOCAL_BIT:
                printf("%sVK_DEPENDENCY_VIEW_LOCAL_BIT\n", indent_level);
                break;
            #endif
            #ifdef VK_EXT_attachment_feedback_loop_layout
            case VK_DEPENDENCY_FEEDBACK_LOOP_BIT_EXT:
                printf("%sVK_DEPENDENCY_FEEDBACK_LOOP_BIT_EXT\n", indent_level);
                break;
            #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkDependencyFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkFramebufferCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT:
                printf("%sVK_FRAMEBUFFER_CREATE_IMAGELESS_BIT\n", indent_level);
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkFramebufferCreateFlagBits: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkImageAspectFlags(uint32_t flag, const char * indent_level) {
    #if defined(VK_VERSION_1_3)
    if (flag == VK_IMAGE_ASPECT_NONE) {
        printf("%sVK_IMAGE_ASPECT_NONE\n", indent_level);
        return;
    }
    #endif
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_IMAGE_ASPECT_COLOR_BIT:
                printf("%sVK_IMAGE_ASPECT_COLOR_BIT\n", indent_level);
                break;
            case VK_IMAGE_ASPECT_DEPTH_BIT:
                printf("%sVK_IMAGE_ASPECT_DEPTH_BIT\n", indent_level);
                break;
            case VK_IMAGE_ASPECT_STENCIL_BIT:
                printf("%sVK_IMAGE_ASPECT_STENCIL_BIT\n", indent_level);
                break;
            case VK_IMAGE_ASPECT_METADATA_BIT:
                printf("%sVK_IMAGE_ASPECT_METADATA_BIT\n", indent_level);
                break;
        #if defined(VK_VERSION_1_1)
            case VK_IMAGE_ASPECT_PLANE_0_BIT:
                printf("%sVK_IMAGE_ASPECT_PLANE_0_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_1)
            case VK_IMAGE_ASPECT_PLANE_1_BIT:
                printf("%sVK_IMAGE_ASPECT_PLANE_1_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_1)
            case VK_IMAGE_ASPECT_PLANE_2_BIT:
                printf("%sVK_IMAGE_ASPECT_PLANE_2_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_image_drm_format_modifier)
            case VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT:
                printf("%sVK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_image_drm_format_modifier)
            case VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT:
                printf("%sVK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_image_drm_format_modifier)
            case VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT:
                printf("%sVK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_image_drm_format_modifier)
            case VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT:
                printf("%sVK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkImageAspectFlagBits: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkImageUsageFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_IMAGE_USAGE_TRANSFER_SRC_BIT:
                printf("%sVK_IMAGE_USAGE_TRANSFER_SRC_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_TRANSFER_DST_BIT:
                printf("%sVK_IMAGE_USAGE_TRANSFER_DST_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_SAMPLED_BIT:
                printf("%sVK_IMAGE_USAGE_SAMPLED_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_STORAGE_BIT:
                printf("%sVK_IMAGE_USAGE_STORAGE_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT:
                printf("%sVK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT:
                printf("%sVK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT:
                printf("%sVK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT\n", indent_level);
                break;
            case VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT:
                printf("%sVK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT\n", indent_level);
                break;
        #if defined(VK_KHR_video_decode_queue)
            case VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_video_decode_queue)
            case VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_video_decode_queue)
            case VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_fragment_density_map)
            case VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT:
                printf("%sVK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_fragment_shading_rate)
            case VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_host_image_copy)
            case VK_IMAGE_USAGE_HOST_TRANSFER_BIT_EXT:
                printf("%sVK_IMAGE_USAGE_HOST_TRANSFER_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_video_encode_queue)
            case VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_video_encode_queue)
            case VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_video_encode_queue)
            case VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR:
                printf("%sVK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_attachment_feedback_loop_layout)
            case VK_IMAGE_USAGE_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT:
                printf("%sVK_IMAGE_USAGE_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_HUAWEI_invocation_mask)
            case VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI:
                printf("%sVK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI\n", indent_level);
                break;
        #endif
        #if defined(VK_QCOM_image_processing)
            case VK_IMAGE_USAGE_SAMPLE_WEIGHT_BIT_QCOM:
                printf("%sVK_IMAGE_USAGE_SAMPLE_WEIGHT_BIT_QCOM\n", indent_level);
                break;
        #endif
        #if defined(VK_QCOM_image_processing)
            case VK_IMAGE_USAGE_SAMPLE_BLOCK_MATCH_BIT_QCOM:
                printf("%sVK_IMAGE_USAGE_SAMPLE_BLOCK_MATCH_BIT_QCOM\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_shading_rate_image)
            case VK_IMAGE_USAGE_SHADING_RATE_IMAGE_BIT_NV:
                printf("%sVK_IMAGE_USAGE_SHADING_RATE_IMAGE_BIT_NV\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkImageUsageFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkImageViewCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            #ifdef VK_EXT_fragment_density_map
            case VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT:
                printf("%sVK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT\n", indent_level);
                break;
            #endif
            #ifdef VK_EXT_descriptor_buffer
            case VK_IMAGE_VIEW_CREATE_DESCRIPTOR_BUFFER_CAPTURE_REPLAY_BIT_EXT:
                printf("%sVK_IMAGE_VIEW_CREATE_DESCRIPTOR_BUFFER_CAPTURE_REPLAY_BIT_EXT\n", indent_level);
                break;
            #endif
            #ifdef VK_EXT_fragment_density_map2
            case VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT:
                printf("%sVK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT\n", indent_level);
                break;
            #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkImageViewCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineRasterizationStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineRasterizationStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineShaderStageCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            #ifdef VK_VERSION_1_3
            case VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT:
                printf("%sVK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT\n", indent_level);
                break;
            case VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT:
                printf("%sVK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT\n", indent_level);
                break;
            #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineShaderStageCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineStageFlags(uint32_t flag, const char * indent_level) {
    #if defined(VK_VERSION_1_3)
    if (flag == VK_PIPELINE_STAGE_NONE) {
        printf("%sVK_PIPELINE_STAGE_NONE\n", indent_level);
        return;
    }
    #endif
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
                
            case VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT:
                printf("%sVK_PIPELINE_STAGE_TOP_OF_PIPE_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT:
                printf("%sVK_PIPELINE_STAGE_DRAW_INDIRECT_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_VERTEX_INPUT_BIT:
                printf("%sVK_PIPELINE_STAGE_VERTEX_INPUT_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_VERTEX_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_VERTEX_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT:
                printf("%sVK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT:
                printf("%sVK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT:
                printf("%sVK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT:
                printf("%sVK_PIPELINE_STAGE_COMPUTE_SHADER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_TRANSFER_BIT:
                printf("%sVK_PIPELINE_STAGE_TRANSFER_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT:
                printf("%sVK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_HOST_BIT:
                printf("%sVK_PIPELINE_STAGE_HOST_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT:
                printf("%sVK_PIPELINE_STAGE_ALL_GRAPHICS_BIT\n", indent_level);
                break;
            case VK_PIPELINE_STAGE_ALL_COMMANDS_BIT:
                printf("%sVK_PIPELINE_STAGE_ALL_COMMANDS_BIT\n", indent_level);
                break;
        #if defined(VK_EXT_transform_feedback)
            case VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT:
                printf("%sVK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_conditional_rendering)
            case VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT:
                printf("%sVK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_acceleration_structure)
            case VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR:
                printf("%sVK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR:
                printf("%sVK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_fragment_density_map)
            case VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT:
                printf("%sVK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_fragment_shading_rate)
            case VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR:
                printf("%sVK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_device_generated_commands)
            case VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_mesh_shader)
            case VK_PIPELINE_STAGE_TASK_SHADER_BIT_EXT:
                printf("%sVK_PIPELINE_STAGE_TASK_SHADER_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_mesh_shader)
            case VK_PIPELINE_STAGE_MESH_SHADER_BIT_EXT:
                printf("%sVK_PIPELINE_STAGE_MESH_SHADER_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_shading_rate_image)
            case VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_mesh_shader)
            case VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_TASK_SHADER_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_mesh_shader)
            case VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV:
                printf("%sVK_PIPELINE_STAGE_MESH_SHADER_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_synchronization2)
            case VK_PIPELINE_STAGE_NONE_KHR:
                printf("%sVK_PIPELINE_STAGE_NONE_KHR\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineStageFlags\e[0m: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineTessellationStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineTessellationStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineVertexInputStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineVertexInputStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineViewportStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineViewportStateCreateFlags\e[0m: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkQueryControlFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
                
            case VK_QUERY_CONTROL_PRECISE_BIT:
                printf("%sVK_QUERY_CONTROL_PRECISE_BIT\n", indent_level);
                break;
                
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkQueryControlFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkQueryPipelineStatisticFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT\n", indent_level);
                break;
            case VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT\n", indent_level);
                break;
        #if defined(VK_EXT_mesh_shader)
            case VK_QUERY_PIPELINE_STATISTIC_TASK_SHADER_INVOCATIONS_BIT_EXT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_TASK_SHADER_INVOCATIONS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_mesh_shader)
            case VK_QUERY_PIPELINE_STATISTIC_MESH_SHADER_INVOCATIONS_BIT_EXT:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_MESH_SHADER_INVOCATIONS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_HUAWEI_cluster_culling_shader)
            case VK_QUERY_PIPELINE_STATISTIC_CLUSTER_CULLING_SHADER_INVOCATIONS_BIT_HUAWEI:
                printf("%sVK_QUERY_PIPELINE_STATISTIC_CLUSTER_CULLING_SHADER_INVOCATIONS_BIT_HUAWEI\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkQueryPipelineStatisticFlags\e[0m: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkRenderPassCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            #ifdef VK_QCOM_render_pass_transform
            case VK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM:
                printf("%sVK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM\n", indent_level);
                break;
            #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkRenderPassCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkSampleCountFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_SAMPLE_COUNT_1_BIT:
                printf("%sVK_SAMPLE_COUNT_1_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_2_BIT:
                printf("%sVK_SAMPLE_COUNT_2_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_4_BIT:
                printf("%sVK_SAMPLE_COUNT_4_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_8_BIT:
                printf("%sVK_SAMPLE_COUNT_8_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_16_BIT:
                printf("%sVK_SAMPLE_COUNT_16_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_32_BIT:
                printf("%sVK_SAMPLE_COUNT_32_BIT\n", indent_level);
                break;
            case VK_SAMPLE_COUNT_64_BIT:
                printf("%sVK_SAMPLE_COUNT_64_BIT\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkSampleCountFlagBits: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkShaderStageFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_SHADER_STAGE_VERTEX_BIT:
                printf("%sVK_SHADER_STAGE_VERTEX_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
                printf("%sVK_SHADER_STAGE_TESSELLATION_CONTROL_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
                printf("%sVK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_GEOMETRY_BIT:
                printf("%sVK_SHADER_STAGE_GEOMETRY_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_FRAGMENT_BIT:
                printf("%sVK_SHADER_STAGE_FRAGMENT_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_COMPUTE_BIT:
                printf("%sVK_SHADER_STAGE_COMPUTE_BIT\n", indent_level);
                break;
            case VK_SHADER_STAGE_ALL_GRAPHICS:
                printf("%sVK_SHADER_STAGE_ALL_GRAPHICS\n", indent_level);
                break;
            case VK_SHADER_STAGE_ALL:
                printf("%sVK_SHADER_STAGE_ALL\n", indent_level);
                break;
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_RAYGEN_BIT_KHR:
                printf("%sVK_SHADER_STAGE_RAYGEN_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_ANY_HIT_BIT_KHR:
                printf("%sVK_SHADER_STAGE_ANY_HIT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR:
                printf("%sVK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_MISS_BIT_KHR:
                printf("%sVK_SHADER_STAGE_MISS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_INTERSECTION_BIT_KHR:
                printf("%sVK_SHADER_STAGE_INTERSECTION_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_SHADER_STAGE_CALLABLE_BIT_KHR:
                printf("%sVK_SHADER_STAGE_CALLABLE_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_mesh_shader)
            case VK_SHADER_STAGE_TASK_BIT_EXT:
                printf("%sVK_SHADER_STAGE_TASK_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_mesh_shader)
            case VK_SHADER_STAGE_MESH_BIT_EXT:
                printf("%sVK_SHADER_STAGE_MESH_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_HUAWEI_subpass_shading)
            case VK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI:
                printf("%sVK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI\n", indent_level);
                break;
        #endif
        #if defined(VK_HUAWEI_cluster_culling_shader)
            case VK_SHADER_STAGE_CLUSTER_CULLING_BIT_HUAWEI:
                printf("%sVK_SHADER_STAGE_CLUSTER_CULLING_BIT_HUAWEI\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_RAYGEN_BIT_NV:
                printf("%sVK_SHADER_STAGE_RAYGEN_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_ANY_HIT_BIT_NV:
                printf("%sVK_SHADER_STAGE_ANY_HIT_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV:
                printf("%sVK_SHADER_STAGE_CLOSEST_HIT_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_MISS_BIT_NV:
                printf("%sVK_SHADER_STAGE_MISS_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_INTERSECTION_BIT_NV:
                printf("%sVK_SHADER_STAGE_INTERSECTION_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_SHADER_STAGE_CALLABLE_BIT_NV:
                printf("%sVK_SHADER_STAGE_CALLABLE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_mesh_shader)
            case VK_SHADER_STAGE_TASK_BIT_NV:
                printf("%sVK_SHADER_STAGE_TASK_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_mesh_shader)
            case VK_SHADER_STAGE_MESH_BIT_NV:
                printf("%sVK_SHADER_STAGE_MESH_BIT_NV\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkShaderStageFlagBits\e[0m: %u\n", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkSubpassDescriptionFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
        #if defined(VK_NVX_multiview_per_view_attributes)
            case VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX:
                printf("%sVK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX\n", indent_level);
                break;
        #endif
        #if defined(VK_NVX_multiview_per_view_attributes)
            case VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX:
                printf("%sVK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX\n", indent_level);
                break;
        #endif
        #if defined(VK_QCOM_render_pass_shader_resolve)
            case VK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM:
                printf("%sVK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM\n", indent_level);
                break;
        #endif
        #if defined(VK_QCOM_render_pass_shader_resolve)
            case VK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM:
                printf("%sVK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_COLOR_ACCESS_BIT_EXT:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_COLOR_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_EXT:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_EXT:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_legacy_dithering)
            case VK_SUBPASS_DESCRIPTION_ENABLE_LEGACY_DITHERING_BIT_EXT:
                printf("%sVK_SUBPASS_DESCRIPTION_ENABLE_LEGACY_DITHERING_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_COLOR_ACCESS_BIT_ARM:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_COLOR_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_ARM:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_ARM:
                printf("%sVK_SUBPASS_DESCRIPTION_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkSubpassDescription: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkSurfaceTransformFlagsKHR(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR\n", indent_level);
                break;
            case VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR:
                printf("%sVK_SURFACE_TRANSFORM_INHERIT_BIT_KHR\n", indent_level);
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkSurfaceTransformFlagBitsKHR: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkSwapchainCreateFlagsKHR(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
        #if defined(VK_VERSION_1_1) && defined(VK_KHR_swapchain) && defined(VK_KHR_device_group) && defined(VK_KHR_swapchain)
            case VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR:
                printf("%sVK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_1) && defined(VK_KHR_swapchain)
            case VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR:
                printf("%sVK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_swapchain_mutable_format)
            case VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR:
                printf("%sVK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_swapchain_maintenance1)
            case VK_SWAPCHAIN_CREATE_DEFERRED_MEMORY_ALLOCATION_BIT_EXT:
                printf("%sVK_SWAPCHAIN_CREATE_DEFERRED_MEMORY_ALLOCATION_BIT_EXT\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkSwapchainCreateFlagBitsKHR\e[0m: %u\nR\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkDeviceCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkDeviceCreateFlags\e[0m: %u\n", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            case VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT:
                printf("%sVK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT\n", indent_level);
                break;
            case VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT:
                printf("%sVK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT\n", indent_level);
                break;
            case VK_PIPELINE_CREATE_DERIVATIVE_BIT:
                printf("%sVK_PIPELINE_CREATE_DERIVATIVE_BIT\n", indent_level);
                break;
        #if defined(VK_VERSION_1_1)
            case VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT:
                printf("%sVK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_1)
            case VK_PIPELINE_CREATE_DISPATCH_BASE_BIT:
                printf("%sVK_PIPELINE_CREATE_DISPATCH_BASE_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_3)
            case VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT:
                printf("%sVK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_VERSION_1_3)
            case VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT:
                printf("%sVK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_dynamic_rendering) && defined(VK_KHR_fragment_shading_rate)
            case VK_PIPELINE_CREATE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_dynamic_rendering) && defined(VK_EXT_fragment_density_map)
            case VK_PIPELINE_CREATE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_ray_tracing_pipeline)
            case VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing)
            case VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV:
                printf("%sVK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_pipeline_executable_properties)
            case VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_pipeline_executable_properties)
            case VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_device_generated_commands)
            case VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV:
                printf("%sVK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_KHR_pipeline_library)
            case VK_PIPELINE_CREATE_LIBRARY_BIT_KHR:
                printf("%sVK_PIPELINE_CREATE_LIBRARY_BIT_KHR\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_descriptor_buffer)
            case VK_PIPELINE_CREATE_DESCRIPTOR_BUFFER_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_DESCRIPTOR_BUFFER_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_graphics_pipeline_library)
            case VK_PIPELINE_CREATE_RETAIN_LINK_TIME_OPTIMIZATION_INFO_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_RETAIN_LINK_TIME_OPTIMIZATION_INFO_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_graphics_pipeline_library)
            case VK_PIPELINE_CREATE_LINK_TIME_OPTIMIZATION_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_LINK_TIME_OPTIMIZATION_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_NV_ray_tracing_motion_blur)
            case VK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_attachment_feedback_loop_layout)
            case VK_PIPELINE_CREATE_COLOR_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_COLOR_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_attachment_feedback_loop_layout)
            case VK_PIPELINE_CREATE_DEPTH_STENCIL_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_DEPTH_STENCIL_ATTACHMENT_FEEDBACK_LOOP_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_opacity_micromap)
            case VK_PIPELINE_CREATE_RAY_TRACING_OPACITY_MICROMAP_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_OPACITY_MICROMAP_BIT_EXT\n", indent_level);
                break;
        #endif
            #ifdef VK_ENABLE_BETA_EXTENSIONS
        #if defined(VK_NV_displacement_micromap)
            case VK_PIPELINE_CREATE_RAY_TRACING_DISPLACEMENT_MICROMAP_BIT_NV:
                printf("%sVK_PIPELINE_CREATE_RAY_TRACING_DISPLACEMENT_MICROMAP_BIT_NV\n", indent_level);
                break;
        #endif
            #endif
        #if defined(VK_EXT_pipeline_protected_access)
            case VK_PIPELINE_CREATE_NO_PROTECTED_ACCESS_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_NO_PROTECTED_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_pipeline_protected_access)
            case VK_PIPELINE_CREATE_PROTECTED_ACCESS_ONLY_BIT_EXT:
                printf("%sVK_PIPELINE_CREATE_PROTECTED_ACCESS_ONLY_BIT_EXT\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineColorBlendStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_PIPELINE_COLOR_BLEND_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_BIT_EXT:
                printf("%sVK_PIPELINE_COLOR_BLEND_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_PIPELINE_COLOR_BLEND_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_BIT_ARM:
                printf("%sVK_PIPELINE_COLOR_BLEND_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
            default:
               fprintf(stderr, "\e[1;31m%sInvalid VkPipelineColorBlendStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineDepthStencilStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_EXT:
                printf("%sVK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_EXT_rasterization_order_attachment_access)
            case VK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_EXT:
                printf("%sVK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_EXT\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_ARM:
                printf("%sVK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_DEPTH_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
        #if defined(VK_ARM_rasterization_order_attachment_access)
            case VK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_ARM:
                printf("%sVK_PIPELINE_DEPTH_STENCIL_STATE_CREATE_RASTERIZATION_ORDER_ATTACHMENT_STENCIL_ACCESS_BIT_ARM\n", indent_level);
                break;
        #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineDepthStencilStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineDynamicStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineDynamicStateCreateFlags\e[0m: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineInputAssemblyStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineInputAssemblyStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}


void DSV_print_flag_VkPipelineLayoutCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            #ifdef VK_EXT_graphics_pipeline_library
            case VK_PIPELINE_LAYOUT_CREATE_INDEPENDENT_SETS_BIT_EXT:
                printf("%sVK_PIPELINE_LAYOUT_CREATE_INDEPENDENT_SETS_BIT_EXT\n", indent_level);
                break;
            #endif
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkImageViewCreateFlags\e[0m: %u\n\e[0m", indent_level, flag);
        }
    }
}

void DSV_print_flag_VkPipelineMultisampleStateCreateFlags(uint32_t flag, const char * indent_level) {
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = flag & (1 << bit);
        switch(mask) {
            case 0:
                break;
            default:
                fprintf(stderr, "\e[1;31m%sInvalid VkPipelineMultisampleStateCreateFlags: %u\n\e[0m", indent_level, flag);
        }
    }
}

#endif
