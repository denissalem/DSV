/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/


#ifdef DEBUG

#include <stdio.h>
#include <string.h>
#include "../debug.h"

void DSV_trace_VkCommandPoolCreateInfo(VkCommandPoolCreateInfo * create_info) {
    printf("DSV: VkCommandPoolCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkCommandPoolCreateFlag(create_info->flags, "\t\t");
    printf("\tqueueFamilyIndex: %u\n", create_info->queueFamilyIndex);
}

void DSV_trace_VkDeviceCreateInfo(VkDeviceCreateInfo * create_info) {
    printf("DSV: VkDeviceCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkDeviceCreateFlags(create_info->flags, "\t\t");
    printf("\tqueueCreateInfoCount: %u\n", create_info->queueCreateInfoCount);
    printf("\tpQueueCreateInfos: %s\n", create_info->pQueueCreateInfos != NULL ? "" : "Not Set");
    for (unsigned i = 0; i < create_info->queueCreateInfoCount; i++) {
        if (create_info->pQueueCreateInfos[i].sType == VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO) {
            printf("\t\tsType: %s\n", "VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO");
        }
        else {
            printf("\t\tsType: \e[1;31m%u\e[0m\n", create_info->pQueueCreateInfos[i].sType);
        }
        printf("\t\tpNext: %lu\n", (unsigned long) create_info->pQueueCreateInfos[i].pNext);
        printf("\t\tflags: %u\n", create_info->pQueueCreateInfos[i].flags);
        printf("\t\tqueueFamilyIndex: %u\n", create_info->pQueueCreateInfos[i].queueFamilyIndex);
        printf("\t\tqueueCount: %u\n", create_info->pQueueCreateInfos[i].queueCount);
        printf("\t\tpQueuePriorities: %s\n", create_info->pQueueCreateInfos[i].pQueuePriorities != NULL ? "" : "Not Set");
        for (unsigned j = 0; j < create_info->pQueueCreateInfos[i].queueCount; j++) {
            printf("\t\t\t%f\n", create_info->pQueueCreateInfos[i].pQueuePriorities[j]);
        }
    }
    printf("\tenabledLayerCount: %u\n", create_info->enabledLayerCount);
    printf("\tppEnabledLayerNames: %s\n", create_info->ppEnabledLayerNames != NULL ? "" : "Not Set");
    for (unsigned i = 0; i < create_info->enabledLayerCount; i++) {
        printf("\t\t%s\n", create_info->ppEnabledLayerNames[i]);
    }
    printf("\tenabledExstensionCount: %u\n", create_info->enabledExtensionCount);
    printf("\tppEnabledExtensionNames: %s\n", create_info->ppEnabledExtensionNames != NULL ? "" : "Not Set");
    for (unsigned i = 0; i < create_info->enabledExtensionCount; i++) {
        printf("\t\t%s\n", create_info->ppEnabledExtensionNames[i]);
    }
    printf("\tpEnabledFeatures: %s\n", create_info->pEnabledFeatures != NULL ? "" : "Not set" );
    if ( create_info->pEnabledFeatures != NULL) {
        printf("\t\trobustBufferAccess: %u\n", create_info->pEnabledFeatures->robustBufferAccess);
        printf("\t\tfullDrawIndexUint32: %u\n", create_info->pEnabledFeatures->fullDrawIndexUint32);
        printf("\t\timageCubeArray: %u\n", create_info->pEnabledFeatures->imageCubeArray);
        printf("\t\tindependentBlend: %u\n", create_info->pEnabledFeatures->independentBlend);
        printf("\t\tgeometryShader: %u\n", create_info->pEnabledFeatures->geometryShader);
        printf("\t\ttessellationShader: %u\n", create_info->pEnabledFeatures->tessellationShader);
        printf("\t\tsampleRateShading: %u\n", create_info->pEnabledFeatures->sampleRateShading);
        printf("\t\tdualSrcBlend: %u\n", create_info->pEnabledFeatures->dualSrcBlend);
        printf("\t\tlogicOp: %u\n", create_info->pEnabledFeatures->logicOp);
        printf("\t\tmultiDrawIndirect: %u\n", create_info->pEnabledFeatures->multiDrawIndirect);
        printf("\t\tdrawIndirectFirstInstance: %u\n", create_info->pEnabledFeatures->drawIndirectFirstInstance);
        printf("\t\tdepthClamp: %u\n", create_info->pEnabledFeatures->depthClamp);
        printf("\t\tdepthBiasClamp: %u\n", create_info->pEnabledFeatures->depthBiasClamp);
        printf("\t\tfillModeNonSolid: %u\n", create_info->pEnabledFeatures->fillModeNonSolid);
        printf("\t\tdepthBounds: %u\n", create_info->pEnabledFeatures->depthBounds);
        printf("\t\twideLines: %u\n", create_info->pEnabledFeatures->wideLines);
        printf("\t\tlargePoints: %u\n", create_info->pEnabledFeatures->largePoints);
        printf("\t\talphaToOne: %u\n", create_info->pEnabledFeatures->alphaToOne);
        printf("\t\tmultiViewport: %u\n", create_info->pEnabledFeatures->multiViewport);
        printf("\t\tsamplerAnisotropy: %u\n", create_info->pEnabledFeatures->samplerAnisotropy);
        printf("\t\ttextureCompressionETC2: %u\n", create_info->pEnabledFeatures->textureCompressionETC2);
        printf("\t\ttextureCompressionASTC_LDR: %u\n", create_info->pEnabledFeatures->textureCompressionASTC_LDR);
        printf("\t\ttextureCompressionBC: %u\n", create_info->pEnabledFeatures->textureCompressionBC);
        printf("\t\tocclusionQueryPrecise: %u\n", create_info->pEnabledFeatures->occlusionQueryPrecise);
        printf("\t\tpipelineStatisticsQuery: %u\n", create_info->pEnabledFeatures->pipelineStatisticsQuery);
        printf("\t\tvertexPipelineStoresAndAtomics: %u\n", create_info->pEnabledFeatures->vertexPipelineStoresAndAtomics);
        printf("\t\tfragmentStoresAndAtomics: %u\n", create_info->pEnabledFeatures->fragmentStoresAndAtomics);
        printf("\t\tshaderTessellationAndGeometryPointSize: %u\n", create_info->pEnabledFeatures->shaderTessellationAndGeometryPointSize);
        printf("\t\tshaderImageGatherExtended: %u\n", create_info->pEnabledFeatures->shaderImageGatherExtended);
        printf("\t\tshaderStorageImageExtendedFormats: %u\n", create_info->pEnabledFeatures->shaderStorageImageExtendedFormats);
        printf("\t\tshaderStorageImageMultisample: %u\n", create_info->pEnabledFeatures->shaderStorageImageMultisample);
        printf("\t\tshaderStorageImageReadWithoutFormat: %u\n", create_info->pEnabledFeatures->shaderStorageImageReadWithoutFormat);
        printf("\t\tshaderStorageImageWriteWithoutFormat: %u\n", create_info->pEnabledFeatures->shaderStorageImageWriteWithoutFormat);
        printf("\t\tshaderUniformBufferArrayDynamicIndexing: %u\n", create_info->pEnabledFeatures->shaderUniformBufferArrayDynamicIndexing);
        printf("\t\tshaderSampledImageArrayDynamicIndexing: %u\n", create_info->pEnabledFeatures->shaderSampledImageArrayDynamicIndexing);
        printf("\t\tshaderStorageBufferArrayDynamicIndexing: %u\n", create_info->pEnabledFeatures->shaderStorageBufferArrayDynamicIndexing);
        printf("\t\tshaderStorageImageArrayDynamicIndexing: %u\n", create_info->pEnabledFeatures->shaderStorageImageArrayDynamicIndexing);
        printf("\t\tshaderClipDistance: %u\n", create_info->pEnabledFeatures->shaderClipDistance);
        printf("\t\tshaderCullDistance: %u\n", create_info->pEnabledFeatures->shaderCullDistance);
        printf("\t\tshaderFloat64: %u\n", create_info->pEnabledFeatures->shaderFloat64);
        printf("\t\tshaderInt64: %u\n", create_info->pEnabledFeatures->shaderInt64);
        printf("\t\tshaderInt16: %u\n", create_info->pEnabledFeatures->shaderInt16);
        printf("\t\tshaderResourceResidency: %u\n", create_info->pEnabledFeatures->shaderResourceResidency);
        printf("\t\tshaderResourceMinLod: %u\n", create_info->pEnabledFeatures->shaderResourceMinLod);
        printf("\t\tsparseBinding: %u\n", create_info->pEnabledFeatures->sparseBinding);
        printf("\t\tsparseResidencyBuffer: %u\n", create_info->pEnabledFeatures->sparseResidencyBuffer);
        printf("\t\tsparseResidencyImage2D: %u\n", create_info->pEnabledFeatures->sparseResidencyImage2D);
        printf("\t\tsparseResidencyImage3D: %u\n", create_info->pEnabledFeatures->sparseResidencyImage3D);
        printf("\t\tsparseResidency2Samples: %u\n", create_info->pEnabledFeatures->sparseResidency2Samples);
        printf("\t\tsparseResidency4Samples: %u\n", create_info->pEnabledFeatures->sparseResidency4Samples);
        printf("\t\tsparseResidency8Samples: %u\n", create_info->pEnabledFeatures->sparseResidency8Samples);
        printf("\t\tsparseResidency16Samples: %u\n", create_info->pEnabledFeatures->sparseResidency16Samples);
        printf("\t\tsparseResidencyAliased: %u\n", create_info->pEnabledFeatures->sparseResidencyAliased);
        printf("\t\tvariableMultisampleRate: %u\n", create_info->pEnabledFeatures->variableMultisampleRate);
        printf("\t\tinheritedQueries: %u\n", create_info->pEnabledFeatures->inheritedQueries);
    }
}

void DSV_trace_VkFramebufferCreateInfo(VkFramebufferCreateInfo * create_info) {
    printf("DSV: VkFramebufferCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkFramebufferCreateFlags(create_info->flags, "\t\t");
    printf("\trenderPass: %lX\n", (long unsigned) create_info->renderPass);
    printf("\tattachmentCount: %u\n", create_info->attachmentCount);
    if (create_info->pAttachments == NULL) {
        printf("\tpAttachments: NULL\n");
    }
    else {
        printf("\tpAttachments:\n");
        for (uint32_t i = 0; i < create_info->attachmentCount; i++) {
                printf("\t\t%lX\n", (long unsigned) create_info->pAttachments[i]);
        }
    }
    printf("\twidth: %u\n", create_info->width);
    printf("\theight: %u\n", create_info->height);
    printf("\tlayers: %u\n", create_info->layers);


}

void DSV_trace_VkGraphicsPipelineCreateInfo(VkGraphicsPipelineCreateInfo * create_info) {
    printf("DSV: VkGraphicsPipelineCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkPipelineCreateFlags(create_info->flags, "\t\t");
    printf("\tstageCount: %u\n", create_info->stageCount);
    printf("\tpStages:\n");
    for (uint32_t i = 0; i < create_info->stageCount; i++) {
        _indent_DSV_trace_VkPipelineShaderStageCreateInfo((VkPipelineShaderStageCreateInfo*) &create_info->pStages[i], "\t");
    }
    
    if (create_info->pVertexInputState) {
        printf("\tpVertexInputState:\n");
        _indent_DSV_trace_VkPipelineVertexInputStateCreateInfo((VkPipelineVertexInputStateCreateInfo*)create_info->pVertexInputState, "\t");
    }
    else {
        printf("\tpVertexInputState: NULL\n");
    }
    
    if (create_info->pInputAssemblyState) {
        printf("\tpInputAssemblyState:\n");
        _indent_DSV_trace_VkPipelineInputAssemblyStateCreateInfo((VkPipelineInputAssemblyStateCreateInfo*)create_info->pInputAssemblyState, "\t");
    }
    else {
        printf("\tpInputAssemblyState: NULL\n");
    }
    
    if (create_info->pTessellationState) {
        printf("\tpTessellationState:\n");
        _indent_DSV_trace_VkPipelineTessellationStateCreateInfo((VkPipelineTessellationStateCreateInfo*) create_info->pInputAssemblyState, "\t");
    }
    else {
        printf("\tpTessellationState: NULL\n");
    }

    if (create_info->pViewportState) {
        printf("\tpViewportState:\n");
        _indent_DSV_trace_VkPipelineViewportStateCreateInfo((VkPipelineViewportStateCreateInfo*) create_info->pViewportState, "\t");
    }
    else {
        printf("\tpViewportState: NULL\n");
    }
    if (create_info->pRasterizationState) {
        printf("\tpRasterizationState:\n");    
        _indent_DSV_trace_VkPipelineRasterizationStateCreateInfo((VkPipelineRasterizationStateCreateInfo*) create_info->pRasterizationState, "\t");
    }
    else {
        printf("\tpRasterizationState: NULL\n");    
    }
    if (create_info->pMultisampleState) {
        printf("\tpMultisampleState:\n");    
        _indent_DSV_trace_VkPipelineMultisampleStateCreateInfo((VkPipelineMultisampleStateCreateInfo*) create_info->pMultisampleState, "\t");
    }
    else {
        printf("\tpMultisampleState: NULL\n");    
    }
    if (create_info->pDepthStencilState) {
        printf("\tpDepthStencilState:\n");    
        _indent_DSV_trace_VkPipelineDepthStencilStateCreateInfo((VkPipelineDepthStencilStateCreateInfo*) create_info->pDepthStencilState, "\t");
    }
    else {
        printf("\tpDepthStencilState: NULL\n");    
    }
    if (create_info->pColorBlendState) {
        printf("\tpColorBlendState:\n");    
        _indent_DSV_trace_VkPipelineColorBlendStateCreateInfo((VkPipelineColorBlendStateCreateInfo*) create_info->pColorBlendState, "\t");
    }
    else {
        printf("\tpColorBlendState: NULL\n");    
    }
    if (create_info->pDynamicState) {
        printf("\tpDynamicState:\n");    
        _indent_DSV_trace_VkPipelineDynamicStateCreateInfo((VkPipelineDynamicStateCreateInfo*) create_info->pDynamicState, "\t");
    }
    else {
        printf("\tpDynamicState: NULL\n");    
    }
    printf("\tlayout: %lX\n", (long unsigned) create_info->layout);
    printf("\trenderPass: %lX\n", (long unsigned) create_info->renderPass);
    printf("\tsubpass: %u\n", create_info->subpass);
    printf("\tbasePipelineHandle: %lX\n", (long unsigned) create_info->basePipelineHandle);
    printf("\tbasePipelineIndex: %u\n", create_info->basePipelineIndex);
}

void DSV_trace_VkImageViewCreateInfo(VkImageViewCreateInfo * create_info) {
    printf("DSV: VkImageViewCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkImageViewCreateFlags(create_info->flags, "\t\t");
    printf("\timage: %lX\n", (long unsigned) create_info->image);
    printf("\tviewType:"); DSV_print_enum_VkImageViewType(create_info->viewType, " ");
    printf("\tformat:"); DSV_print_enum_VkFormat(create_info->format, " ");
    printf("\tcomponents:\n");
    printf("\t\tr:"); DSV_print_enum_VkComponentSwizzle(create_info->components.r, " ");
    printf("\t\tg:"); DSV_print_enum_VkComponentSwizzle(create_info->components.g, " ");
    printf("\t\tb:"); DSV_print_enum_VkComponentSwizzle(create_info->components.b, " ");
    printf("\t\ta:"); DSV_print_enum_VkComponentSwizzle(create_info->components.a, " ");
    printf("\tsubresourceRange:\n");
    printf("\t\taspectMask:\n"); DSV_print_flag_VkImageAspectFlags(create_info->subresourceRange.aspectMask, "\t\t\t");
    printf("\t\tbaseMipLevel: %u\n", create_info->subresourceRange.baseMipLevel);
    printf("\t\tlevelCount: %u\n", create_info->subresourceRange.levelCount);
    printf("\t\tbaseArrayLayer: %u\n", create_info->subresourceRange.baseArrayLayer);
    printf("\t\tlayerCount: %u\n", create_info->subresourceRange.layerCount);
}

void DSV_trace_VkPipelineColorBlendStateCreateInfo(VkPipelineColorBlendStateCreateInfo* create_info) {
    _indent_DSV_trace_VkPipelineColorBlendStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineDepthStencilStateCreateInfo(VkPipelineDepthStencilStateCreateInfo* create_info) {
    _indent_DSV_trace_VkPipelineDepthStencilStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineDynamicStateCreateInfo(VkPipelineDynamicStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineDynamicStateCreateInfo(create_info, "");
}
void DSV_trace_VkPipelineInputAssemblyStateCreateInfo(VkPipelineInputAssemblyStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineInputAssemblyStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineLayoutCreateInfo(VkPipelineLayoutCreateInfo * create_info) {
    printf("DSV: VkPipelineLayoutCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (unsigned long) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkPipelineLayoutCreateFlags(create_info->flags, "\t\t");
    printf("\tsetLayoutCount: %u\n", create_info->setLayoutCount);
    if (create_info->pushConstantRangeCount == 0) {
        printf("\tpSetLayouts: NULL\n");
    }
    else {
        printf("\tpSetLayouts:\n");
        for (uint32_t i = 0; i < create_info->pushConstantRangeCount; i++) {
            printf("\t\t%lX\n", (unsigned long) create_info->pSetLayouts[i]);
        }
    }
    printf("\tpushConstantRangeCount: %u\n", create_info->pushConstantRangeCount);
    printf("\tpPushConstantRanges:\n");
    for (uint32_t i = 0; i < create_info->pushConstantRangeCount; i++) {
            printf("\t\t\tstageFlags:\n"); DSV_print_flag_VkShaderStageFlags(create_info->pPushConstantRanges[i].stageFlags,"\t\t\t");
            printf("\t\t\toffset: %u\n", create_info->pPushConstantRanges[i].offset);
            printf("\t\t\tsize: %u\n", create_info->pPushConstantRanges[i].size);
            printf("\n");
    }
}

void DSV_trace_VkPipelineMultisampleStateCreateInfo(VkPipelineMultisampleStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineMultisampleStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineRasterizationStateCreateInfo(VkPipelineRasterizationStateCreateInfo* create_info) {
    _indent_DSV_trace_VkPipelineRasterizationStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineShaderStageCreateInfo(VkPipelineShaderStageCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineShaderStageCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineTessellationStateCreateInfo(VkPipelineTessellationStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineTessellationStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineVertexInputStateCreateInfo(VkPipelineVertexInputStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineVertexInputStateCreateInfo(create_info, "");
}

void DSV_trace_VkPipelineViewportStateCreateInfo(VkPipelineViewportStateCreateInfo * create_info) {
    _indent_DSV_trace_VkPipelineViewportStateCreateInfo(create_info, "");
}

void DSV_trace_VkRenderPassCreateInfo(VkRenderPassCreateInfo * create_info) {
    printf("DSV: VkRenderPassCreateInfo has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (long unsigned) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkRenderPassCreateFlags(create_info->flags, "\t\t");
    printf("\tattachmentCount: %u\n", create_info->attachmentCount);
    printf("\tpAttachments:\n");
    if (create_info->pAttachments != NULL) {
        for (uint32_t i = 0; i < create_info->attachmentCount; i++) {
            printf("\t\tflags:\n"); DSV_print_flag_VkAttachmentDescriptionFlags(create_info->pAttachments[i].flags, "\t\t\t");
            printf("\t\tformat:"); DSV_print_enum_VkFormat(create_info->pAttachments[i].format, " ");
            printf("\t\tsamples:\n"); DSV_print_flag_VkSampleCountFlags(create_info->pAttachments[i].samples, "\t\t\t");
            printf("\t\tloadOp:"); DSV_print_enum_VkAttachmentLoadOp(create_info->pAttachments[i].loadOp, " ");
            printf("\t\tstoreOp:"); DSV_print_enum_VkAttachmentStoreOp(create_info->pAttachments[i].storeOp, " ");
            printf("\t\tstencilLoadOp:"); DSV_print_enum_VkAttachmentLoadOp(create_info->pAttachments[i].stencilLoadOp, " ");
            printf("\t\tstencilStoreOp:"); DSV_print_enum_VkAttachmentStoreOp(create_info->pAttachments[i].stencilStoreOp, " ");
            printf("\t\tinitialLayout:"); DSV_print_enum_VkImageLayout(create_info->pAttachments[i].initialLayout," ");
            printf("\t\tfinalLayout:"); DSV_print_enum_VkImageLayout(create_info->pAttachments[i].finalLayout, " ");
            printf("\n"); 
        }
    }
    else if (create_info->attachmentCount > 0) {
        printf("\t\t\e[1;31mattachmentCount is %u but pAttachments is NULL\e[0m\n", create_info->attachmentCount);
    }
    printf("\tsubpassCount: %u\n", create_info->subpassCount);
    printf("\tpSubpasses:\n");
    if (create_info->pSubpasses != NULL) {
        for (uint32_t i = 0; i < create_info->subpassCount; i++) {
            printf("\t\tflags:\n"); DSV_print_flag_VkSubpassDescriptionFlags(create_info->pSubpasses[i].flags, "\t\t\t");
            printf("\t\tpipelineBindPoint:"); DSV_print_enum_VkPipelineBindPoint(create_info->pSubpasses[i].pipelineBindPoint, " ");
            printf("\t\tinputAttachmentCount: %u\n", create_info->pSubpasses[i].inputAttachmentCount);
            printf("\t\tpInputAttachments:\n");
            for (uint32_t j = 0; j < create_info->pSubpasses[i].inputAttachmentCount; j++) {
                printf("\t\t\tattachment: %u\n", create_info->pSubpasses[i].pInputAttachments[j].attachment);
                printf("\t\t\tlayout:"); DSV_print_enum_VkImageLayout(create_info->pSubpasses[i].pInputAttachments[j].layout, " ");
                printf("\n");
            }
            printf("\t\tcolorAttachmentCount: %u\n", create_info->pSubpasses[i].colorAttachmentCount);
            printf("\t\tpColorAttachments:\n");
            if (create_info->pSubpasses[i].pColorAttachments != NULL) {
                for (uint32_t j = 0; j < create_info->pSubpasses[i].colorAttachmentCount; j++) {
                    printf("\t\t\tattachment: %u\n", create_info->pSubpasses[i].pColorAttachments[j].attachment);
                    printf("\t\t\tlayout:"); DSV_print_enum_VkImageLayout(create_info->pSubpasses[i].pColorAttachments[j].layout, " ");
                    printf("\n");
                }
            }
            else if (create_info->pSubpasses[i].colorAttachmentCount > 0) {
                printf("\t\t\t\e[1;31mcolorAttachmentCount is %u but pColorAttachments is NULL\e[0m\n", create_info->pSubpasses[i].colorAttachmentCount);
            }
            printf("\t\tpResolveAttachments:\n");
            if (create_info->pSubpasses[i].pResolveAttachments != NULL) {
                for (uint32_t j = 0; j < create_info->pSubpasses[i].colorAttachmentCount; j++) {
                    printf("\t\t\tattachment: %u\n", create_info->pSubpasses[i].pResolveAttachments[j].attachment);
                    printf("\t\t\tlayout:"); DSV_print_enum_VkImageLayout(create_info->pSubpasses[i].pResolveAttachments[j].layout, " ");
                    printf("\n");
                }
            }
            printf("\t\tpDepthStencilAttachment:\n");
            if (create_info->pSubpasses[i].pDepthStencilAttachment != NULL) {
                for (uint32_t j = 0; j < create_info->pSubpasses[i].colorAttachmentCount; j++) {
                    printf("\t\t\tattachment: %u\n", create_info->pSubpasses[i].pDepthStencilAttachment[j].attachment);
                    printf("\t\t\tlayout:"); DSV_print_enum_VkImageLayout(create_info->pSubpasses[i].pDepthStencilAttachment[j].layout, " ");
                    printf("\n");
                }
            }
            printf("\t\tpreserveAttachmentCount: %u\n", create_info->pSubpasses[i].preserveAttachmentCount);
            printf("\t\tpPreserveAttachments: \n");
            if (create_info->pSubpasses[i].pPreserveAttachments != NULL) {
                for (uint32_t j = 0; j < create_info->pSubpasses[i].preserveAttachmentCount; j++) {
                    printf("\t\t\t%u\n", create_info->pSubpasses[i].pPreserveAttachments[j]);
                }
            }
            else if (create_info->pSubpasses[i].preserveAttachmentCount > 0) {
                printf("\t\t\t\e[1;31mpreserveAttachmentCount is %u but pPreserveAttachments is NULL\e[0m\n", create_info->pSubpasses[i].preserveAttachmentCount);
            }
            printf("\n");
        }
    }
    else if (create_info->subpassCount > 0) {
        printf("\t\t\e[1;31msubpassCount is %u but pSubpasses is NULL\e[0m\n", create_info->subpassCount);
    }
    printf("\tdependencyCount: %u\n", create_info->dependencyCount);
    printf("\tpDependencies:\n");
    if (create_info->pDependencies != NULL) {
        for (uint32_t i = 0; i < create_info->subpassCount; i++) {
            if (create_info->pDependencies[i].srcSubpass == VK_SUBPASS_EXTERNAL) {
                printf("\t\tsrcSubpass: VK_SUBPASS_EXTERNAL\n");
            }
            else {
                printf("\t\tsrcSubpass: %u\n", create_info->pDependencies[i].srcSubpass);
            }
            if (create_info->pDependencies[i].dstSubpass == VK_SUBPASS_EXTERNAL) {
                printf("\t\tdstSubpass: VK_SUBPASS_EXTERNAL\n");
            }
            else {
                printf("\t\tdstSubpass: %u\n", create_info->pDependencies[i].dstSubpass);
            }
            printf("\t\tsrcStageMask:\n"); DSV_print_flag_VkPipelineStageFlags(create_info->pDependencies[i].srcStageMask, "\t\t\t");
            printf("\t\tdstStageMask:\n"); DSV_print_flag_VkPipelineStageFlags(create_info->pDependencies[i].dstStageMask, "\t\t\t");
            printf("\t\tsrcAccessMask:\n"); DSV_print_flag_VkAccessFlags(create_info->pDependencies[i].srcAccessMask, "\t\t\t");
            printf("\t\tdstAccessMask:\n"); DSV_print_flag_VkAccessFlags(create_info->pDependencies[i].dstAccessMask, "\t\t\t");
            printf("\t\tdependencyFlags:\n"); DSV_print_flag_VkDependencyFlags(create_info->pDependencies[i].dependencyFlags, "\t\t\t");
        }
    }
    else if (create_info->dependencyCount > 0) {
        printf("\t\t\e[1;31mdependencyCount is %u but pDependencies is NULL\e[0m\n", create_info->dependencyCount);
    }
}

void DSV_trace_VkSwapchainCreateInfoKHR(VkSwapchainCreateInfoKHR * create_info) {
    printf("DSV: VkSwapchainCreateInfoKHR has the following values:\n");
    printf("\tsType:"); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("\tpNext: %lu\n", (long unsigned) create_info->pNext);
    printf("\tflags:\n"); DSV_print_flag_VkSwapchainCreateFlagsKHR(create_info->flags, "\t\t");
    printf("\tsurface %lX\n", (unsigned long) create_info->surface);
    printf("\tminImageCount: %u\n",  create_info->minImageCount);
    printf("\timageFormat:"); DSV_print_enum_VkFormat(create_info->imageFormat, " ");
    printf("\timageColorSpace:"); DSV_print_enum_VkColorSpaceKHR(create_info->imageColorSpace, " ");
    printf("\timageExtent:\n");
    printf("\t\twidth: %u\n", create_info->imageExtent.width);
    printf("\t\theight: %u\n", create_info->imageExtent.height);
    printf("\timageArrayLayers: %u\n", create_info->imageArrayLayers);
    printf("\timageUsage:\n"); DSV_print_flag_VkImageUsageFlags(create_info->imageUsage,"\t\t");
    if (create_info->imageSharingMode == VK_SHARING_MODE_EXCLUSIVE) {
        printf("\timageSharingMode: %s\n", "VK_SHARING_MODE_EXCLUSIVE");
    }
    else if (create_info->imageSharingMode == VK_SHARING_MODE_CONCURRENT) {
        printf("\timageSharingMode: %s\n", "VK_SHARING_MODE_CONCURRENT");
    }
    else {
        printf("\timageSharingMode: %s\n", "\e[1;31mInvalid value\e[0m");
    }
    printf("\tqueueFamilyIndexCount: %u\n", create_info->queueFamilyIndexCount);
    if (create_info->queueFamilyIndexCount == 0) {
        printf("\tpQueueFamilyIndices: NULL\n");
    }
    else {
        printf("\tpQueueFamilyIndices:\n");
        for (unsigned i = 0; i < create_info->queueFamilyIndexCount; i++) {
            printf("\t\t%u\n", create_info->pQueueFamilyIndices[i]);
        }
    }
    printf("\tpreTransform:\n"); DSV_print_flag_VkSurfaceTransformFlagsKHR(create_info->preTransform, "\t\t");
    printf("\tcompositeAlpha:\n"); DSV_print_flag_VkCompositeAlphaFlagsKHR(create_info->compositeAlpha, "\t\t");
    printf("\tpresentMode:"); DSV_print_enum_VkPresentModeKHR(create_info->presentMode, " ");
    printf("\tclipped: %u\n", create_info->clipped);
    printf("\toldSwapchain: %lX\n", (long unsigned) create_info->oldSwapchain);
}

void _indent_DSV_trace_VkPipelineColorBlendStateCreateInfo(VkPipelineColorBlendStateCreateInfo* create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineColorBlendStateCreateInfo has the following values:\n");
    }

    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineColorBlendStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tlogicOpEnable:", indent); DSV_print_enum_VkBool32(create_info->logicOpEnable, " ");
    printf("%s\tlogicOp:", indent); DSV_print_enum_VkLogicOp(create_info->logicOp, " ");
    printf("%s\tattachmentCount: %u\n", indent, create_info->attachmentCount);
    if (create_info->attachmentCount == 0) {
        printf("%s\tpAttachments: NULL\n", indent);
    }
    else {
        printf("%s\tpAttachments:\n", indent);
        for (uint32_t i = 0; i < create_info->attachmentCount; i++) {
            _indent_DSV_trace_VkPipelineColorBlendAttachmentState((VkPipelineColorBlendAttachmentState*)&create_info->pAttachments[i], indent_extra);
        }
    } 
    printf("%s\tblendConstants:\n", indent);
    printf("%s\t\t%f\n", indent, create_info->blendConstants[0]);
    printf("%s\t\t%f\n", indent, create_info->blendConstants[1]);
    printf("%s\t\t%f\n", indent, create_info->blendConstants[2]);
    printf("%s\t\t%f\n", indent, create_info->blendConstants[3]);
}

void _indent_DSV_trace_VkPipelineDepthStencilStateCreateInfo(VkPipelineDepthStencilStateCreateInfo* create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineDepthStencilStateCreateInfo has the following values:\n");
    }
    
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineDepthStencilStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tdepthTestEnable:", indent); DSV_print_enum_VkBool32(create_info->depthTestEnable, " ");
    printf("%s\tdepthWriteEnable:", indent); DSV_print_enum_VkBool32(create_info->depthWriteEnable, " ");
    printf("%s\tdepthCompareOp:", indent); DSV_print_enum_VkCompareOp(create_info->depthCompareOp, " ");
    printf("%s\tdepthBoundsTestEnable:", indent); DSV_print_enum_VkBool32(create_info->depthBoundsTestEnable, " ");
    printf("%s\tstencilTestEnable:", indent); DSV_print_enum_VkBool32(create_info->stencilTestEnable, " ");
    printf("%s\tfront:\n", indent); _indent_DSV_trace_VkStencilOpState(&create_info->front, indent_extra);
    printf("%s\tback:\n", indent); _indent_DSV_trace_VkStencilOpState(&create_info->back, indent_extra);
    printf("%s\tminDepthBounds: %f\n", indent, create_info->minDepthBounds);
    printf("%s\tmaxDepthBounds: %f\n", indent, create_info->maxDepthBounds);
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineDynamicStateCreateInfo(VkPipelineDynamicStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineDynamicStateCreateInfo has the following values:\n");
    }
    
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineDynamicStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tdynamicStateCount: %u\n", indent, create_info->dynamicStateCount);
    if (create_info->pDynamicStates != NULL) {
        printf("%s\tpDynamicStates:\n", indent);
        for (uint32_t i = 0; i < create_info->dynamicStateCount; i++) {
            printf("%s\t\t", indent); DSV_print_enum_VkDynamicState(create_info->pDynamicStates[i], " ");
        }
    }
    else {
        printf("%s\tpDynamicStates: NULL\n", indent);
    }
}

void _indent_DSV_trace_VkPipelineInputAssemblyStateCreateInfo(VkPipelineInputAssemblyStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineInputAssemblyStateCreateInfo has the following values:\n");
    }
    
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineInputAssemblyStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\ttopology:", indent); DSV_print_enum_VkPrimitiveTopology(create_info->topology, " ");
    printf("%s\tprimitiveRestartEnable:", indent); DSV_print_enum_VkBool32(create_info->primitiveRestartEnable, " ");
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineMultisampleStateCreateInfo(VkPipelineMultisampleStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV:VkPipelineMultisampleStateCreateInfo has the following values:\n");
    }
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent);
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineMultisampleStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\trasterizationSamples:\n", indent); DSV_print_flag_VkSampleCountFlags(create_info->rasterizationSamples, indent_extra);
    printf("%s\tsampleShadingEnable", indent); DSV_print_enum_VkBool32(create_info->sampleShadingEnable, " ");
    printf("%s\tminSampleShading: %f\n", indent, create_info->minSampleShading);
    if (create_info->pSampleMask != NULL) {
        printf("%s\tpSampleMask:\n", indent);
        for (uint32_t i = 0; i < create_info->rasterizationSamples < 33 ? 1 : 2; i++) {
            printf("%s\t\t", indent);
            for (uint32_t j = 0; j < 32; j++) {
                printf("%u", (1 << j) & create_info->pSampleMask[i]);
            }
            printf("\n");
        }
    }
    else{
        printf("%s\tpSampleMask: NULL\n", indent);
    }
    printf("%s\talphaToCoverageEnable:", indent); DSV_print_enum_VkBool32(create_info->alphaToCoverageEnable, " ");
    printf("%s\talphaToOneEnable:", indent); DSV_print_enum_VkBool32(create_info->alphaToOneEnable, " ");
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineRasterizationStateCreateInfo(VkPipelineRasterizationStateCreateInfo* create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineRasterizationStateCreateInfo has the following values:\n");
    }
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineRasterizationStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tdepthClampEnable:", indent); DSV_print_enum_VkBool32(create_info->depthClampEnable, " ");
    printf("%s\trasterizerDiscardEnable:", indent); DSV_print_enum_VkBool32(create_info->rasterizerDiscardEnable, " ");
    printf("%s\tpolygonMode:", indent); DSV_print_enum_VkPolygonMode(create_info->polygonMode, " ");
    printf("%s\tcullMode", indent); DSV_print_enum_VkCullModeFlags(create_info->cullMode, " ");
    printf("%s\tfrontFace:", indent); DSV_print_enum_VkFrontFace(create_info->frontFace, " ");
    printf("%s\tdepthBiasEnable:", indent); DSV_print_enum_VkBool32(create_info->depthBiasEnable, " ");
    printf("%s\tdepthBiasConstantFactor: %f\n", indent, create_info->depthBiasConstantFactor);
    printf("%s\tdepthBiasClamp: %f\n", indent, create_info->depthBiasClamp);
    printf("%s\tdepthBiasSlopeFactor: %f\n", indent, create_info->depthBiasSlopeFactor);
    printf("%s\tlineWidth: %f\n", indent, create_info->lineWidth);

    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineShaderStageCreateInfo(VkPipelineShaderStageCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineShaderStageCreateInfo has the following values:\n");
    }
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineShaderStageCreateFlags(create_info->flags, indent_extra);
    printf("%s\tstage:\n", indent); DSV_print_flag_VkShaderStageFlags(create_info->stage, indent_extra);
    printf("%s\tmodule: %lX\n", indent, (long unsigned) create_info->module);
    printf("%s\tpName: %s\n", indent, create_info->pName);
    printf("%s\tpSpecializationInfo: %s\n", indent, create_info->pSpecializationInfo == NULL ? "None" : "");
    if (create_info->pSpecializationInfo != NULL) {
        printf("%s\t\tmapEntryCount: %u\n", indent, create_info->pSpecializationInfo->mapEntryCount);
        printf("%s\t\tpMapEntries:\n", indent);
        for (uint32_t i = 0; i < create_info->pSpecializationInfo->mapEntryCount; i++) {
            printf("%s\t\t\tconstantID: %u\n", indent, create_info->pSpecializationInfo->pMapEntries[i].constantID);
            printf("%s\t\t\toffset: %u\n", indent, create_info->pSpecializationInfo->pMapEntries[i].offset);
            printf("%s\t\t\tsize: %lu\n", indent, create_info->pSpecializationInfo->pMapEntries[i].size);
            printf("\n");
        }
        printf("%s\t\tdataSize: %lu\n", indent, create_info->pSpecializationInfo->dataSize);
        printf("%s\t\tpData: %lu\n", indent, (unsigned long) create_info->pSpecializationInfo->pData);
    }
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineTessellationStateCreateInfo(VkPipelineTessellationStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineTessellationStateCreateInfo has the following values:\n");
    }
    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineTessellationStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tpatchControlPoints: %u\n", indent, create_info->patchControlPoints); 
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineVertexInputStateCreateInfo(VkPipelineVertexInputStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineVertexInputStateCreateInfo has the following values:\n");
    }

    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineVertexInputStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tvertexBindingDescriptionCount: %u\n", indent, create_info->vertexBindingDescriptionCount);
    printf("%s\tpVertexBindingDescriptions:\n", indent);
    for (uint32_t i = 0; i < create_info->vertexBindingDescriptionCount; i++) {
        printf("%s\tbinding: %u\n", indent, create_info->pVertexBindingDescriptions[i].binding);
        printf("%s\tstride: %u\n", indent, create_info->pVertexBindingDescriptions[i].stride);
        printf("%s\tinputRate:", indent); DSV_print_enum_VkVertexInputRate( create_info->pVertexBindingDescriptions[i].inputRate, " ");
        printf("\n");
    }
    printf("%s\tvertexAttributeDescriptionCount: %u\n", indent, create_info->vertexAttributeDescriptionCount);
    printf("%s\tpVertexAttributeDescriptions:\n", indent);
    for (uint32_t i = 0; i < create_info->vertexAttributeDescriptionCount; i++) {
        printf("%s\tlocation: %u\n", indent, create_info->pVertexAttributeDescriptions[i].location);
        printf("%s\tbinding: %u\n", indent, create_info->pVertexAttributeDescriptions[i].binding);
        printf("%s\tformat:", indent); DSV_print_enum_VkFormat(create_info->pVertexAttributeDescriptions[i].format, " ");
        printf("%s\toffset: %u\n", indent, create_info->pVertexAttributeDescriptions[i].offset);
        printf("\n");
    }
    if (strlen(indent)) {
        printf("\n");
    }
}

void _indent_DSV_trace_VkPipelineViewportStateCreateInfo(VkPipelineViewportStateCreateInfo * create_info, const char * indent) {
    if (strlen(indent) == 0) {
        printf("DSV: VkPipelineViewportStateCreateInfo has the following values:\n");
    }

    printf("%s\tsType:", indent); DSV_print_enum_VkStructureType(create_info->sType, " ");
    printf("%s\tpNext: %lu\n", indent, (unsigned long) create_info->pNext);
    char indent_extra[16] = {0};
    strcat(&indent_extra[0], "\t\t"); 
    strcat(&indent_extra[0], indent); 
    printf("%s\tflags:\n", indent); DSV_print_flag_VkPipelineViewportStateCreateFlags(create_info->flags, indent_extra);
    printf("%s\tviewportCount: %u\n", indent, create_info->viewportCount);
    printf("%s\tpViewports:\n", indent);
    for (uint32_t i = 0; i < create_info->viewportCount; i++) {
        printf("%s\t\tx: %f\n", indent, create_info->pViewports[i].x);
        printf("%s\t\ty: %f\n", indent, create_info->pViewports[i].y);
        printf("%s\t\twidth: %f\n", indent, create_info->pViewports[i].width);
        printf("%s\t\theight: %f\n", indent, create_info->pViewports[i].height);
        printf("%s\t\tminDepth: %f\n", indent, create_info->pViewports[i].minDepth);
        printf("%s\t\tmaxDepth: %f\n", indent, create_info->pViewports[i].maxDepth);
        printf("\n");
    }
    printf("%s\tscissorCount: %u\n", indent, create_info->scissorCount);
    printf("%s\tpScissors:\n", indent);
    for (uint32_t i = 0; i < create_info->scissorCount; i++) {
        printf("%s\t\toffset:\n", indent);
        printf("%s\t\t\tx: %d\n", indent, create_info->pScissors[i].offset.x);
        printf("%s\t\t\ty: %d\n", indent, create_info->pScissors[i].offset.y);
        printf("%s\t\textent:\n", indent);
        printf("%s\t\t\twidth: %d\n", indent, create_info->pScissors[i].extent.width);
        printf("%s\t\t\theight: %d\n", indent, create_info->pScissors[i].extent.height);
        printf("\n");
    }
    
    if (strlen(indent)) {
        printf("\n");
    }
}

#endif
