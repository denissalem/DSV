/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "../../debug.h"

uint32_t DSV_VkResult_to_string(uint32_t value, char * string) {
    switch(value) {
        case VK_SUCCESS:
            sprintf(string, "VK_SUCCESS");
            break;
        case VK_NOT_READY:
            sprintf(string, "VK_NOT_READY");
            break;
        case VK_TIMEOUT:
            sprintf(string, "VK_TIMEOUT");
            break;
        case VK_EVENT_SET:
            sprintf(string, "VK_EVENT_SET");
            break;
        case VK_EVENT_RESET:
            sprintf(string, "VK_EVENT_RESET");
            break;
        case VK_INCOMPLETE:
            sprintf(string, "VK_INCOMPLETE");
            break;
        case VK_ERROR_OUT_OF_HOST_MEMORY:
            sprintf(string, "VK_ERROR_OUT_OF_HOST_MEMORY");
            break;
        case VK_ERROR_OUT_OF_DEVICE_MEMORY:
            sprintf(string, "VK_ERROR_OUT_OF_DEVICE_MEMORY");
            break;
        case VK_ERROR_INITIALIZATION_FAILED:
            sprintf(string, "VK_ERROR_INITIALIZATION_FAILED");
            break;
        case VK_ERROR_DEVICE_LOST:
            sprintf(string, "VK_ERROR_DEVICE_LOST");
            break;
        case VK_ERROR_MEMORY_MAP_FAILED:
            sprintf(string, "VK_ERROR_MEMORY_MAP_FAILED");
            break;
        case VK_ERROR_LAYER_NOT_PRESENT:
            sprintf(string, "VK_ERROR_LAYER_NOT_PRESENT");
            break;
        case VK_ERROR_EXTENSION_NOT_PRESENT:
            sprintf(string, "VK_ERROR_EXTENSION_NOT_PRESENT");
            break;
        case VK_ERROR_FEATURE_NOT_PRESENT:
            sprintf(string, "VK_ERROR_FEATURE_NOT_PRESENT");
            break;
        case VK_ERROR_INCOMPATIBLE_DRIVER:
            sprintf(string, "VK_ERROR_INCOMPATIBLE_DRIVER");
            break;
        case VK_ERROR_TOO_MANY_OBJECTS:
            sprintf(string, "VK_ERROR_TOO_MANY_OBJECTS");
            break;
        case VK_ERROR_FORMAT_NOT_SUPPORTED:
            sprintf(string, "VK_ERROR_FORMAT_NOT_SUPPORTED");
            break;
        case VK_ERROR_FRAGMENTED_POOL:
            sprintf(string, "VK_ERROR_FRAGMENTED_POOL");
            break;
        case VK_ERROR_UNKNOWN:
            sprintf(string, "VK_ERROR_UNKNOWN");
            break;
        #if defined(VK_KHR_surface)
        case VK_ERROR_SURFACE_LOST_KHR:
            sprintf(string, "VK_ERROR_SURFACE_LOST_KHR");
            break;
        #endif
        #if defined(VK_KHR_surface)
        case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
            sprintf(string, "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR");
            break;
        #endif
        #if defined(VK_KHR_swapchain)
        #if defined(VK_KHR_surface)
        case VK_SUBOPTIMAL_KHR:
            sprintf(string, "VK_SUBOPTIMAL_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_swapchain)
        #if defined(VK_KHR_surface)
        case VK_ERROR_OUT_OF_DATE_KHR:
            sprintf(string, "VK_ERROR_OUT_OF_DATE_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_display_swapchain)
        #if defined(VK_KHR_swapchain) && defined(VK_KHR_display)
        case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
            sprintf(string, "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR");
            break;
        #endif
        #endif
        #if defined(VK_EXT_debug_report)
        case VK_ERROR_VALIDATION_FAILED_EXT:
            sprintf(string, "VK_ERROR_VALIDATION_FAILED_EXT");
            break;
        #endif
        #if defined(VK_NV_glsl_shader)
        case VK_ERROR_INVALID_SHADER_NV:
            sprintf(string, "VK_ERROR_INVALID_SHADER_NV");
            break;
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_queue)
        #if  ( defined(VK_VERSION_1_1) && defined(VK_KHR_synchronization2) )  || defined(VK_VERSION_1_3)
        case VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR:
            sprintf(string, "VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR");
            break;
        #endif
        #endif
        #if defined(VK_KHR_maintenance1)
        case VK_ERROR_OUT_OF_POOL_MEMORY_KHR:
            sprintf(string, "VK_ERROR_OUT_OF_POOL_MEMORY_KHR");
            break;
        #endif
        #if defined(VK_KHR_external_memory)
        #if defined(VK_KHR_external_memory_capabilities) || defined(VK_VERSION_1_1)
        case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR:
            sprintf(string, "VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR");
            break;
        #endif
        #endif
        #if defined(VK_EXT_image_drm_format_modifier)
        #if  (  (  ( defined(VK_KHR_bind_memory2) && defined(VK_KHR_get_physical_device_properties2) && defined(VK_KHR_sampler_ycbcr_conversion) )  || defined(VK_VERSION_1_1) )  && defined(VK_KHR_image_format_list) )  || defined(VK_VERSION_1_2)
        case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT:
            sprintf(string, "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT");
            break;
        #endif
        #endif
        #if defined(VK_EXT_descriptor_indexing)
        #if  ( defined(VK_KHR_get_physical_device_properties2) && defined(VK_KHR_maintenance3) )  || defined(VK_VERSION_1_1)
        case VK_ERROR_FRAGMENTATION_EXT:
            sprintf(string, "VK_ERROR_FRAGMENTATION_EXT");
            break;
        #endif
        #endif
        #if defined(VK_EXT_global_priority)
        case VK_ERROR_NOT_PERMITTED_EXT:
            sprintf(string, "VK_ERROR_NOT_PERMITTED_EXT");
            break;
        #endif
        #if defined(VK_EXT_buffer_device_address)
        #if defined(VK_KHR_get_physical_device_properties2) || defined(VK_VERSION_1_1)
        case VK_ERROR_INVALID_DEVICE_ADDRESS_EXT:
            sprintf(string, "VK_ERROR_INVALID_DEVICE_ADDRESS_EXT");
            break;
        #endif
        #endif
        #if defined(VK_EXT_full_screen_exclusive)
        #if  ( defined(VK_KHR_get_physical_device_properties2) || defined(VK_VERSION_1_1) )  && defined(VK_KHR_surface) && defined(VK_KHR_get_surface_capabilities2) && defined(VK_KHR_swapchain)
        case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT:
            sprintf(string, "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT");
            break;
        #endif
        #endif
        #if defined(VK_KHR_deferred_host_operations)
        case VK_THREAD_IDLE_KHR:
            sprintf(string, "VK_THREAD_IDLE_KHR");
            break;
        #endif
        #if defined(VK_KHR_deferred_host_operations)
        case VK_THREAD_DONE_KHR:
            sprintf(string, "VK_THREAD_DONE_KHR");
            break;
        #endif
        #if defined(VK_KHR_deferred_host_operations)
        case VK_OPERATION_DEFERRED_KHR:
            sprintf(string, "VK_OPERATION_DEFERRED_KHR");
            break;
        #endif
        #if defined(VK_KHR_deferred_host_operations)
        case VK_OPERATION_NOT_DEFERRED_KHR:
            sprintf(string, "VK_OPERATION_NOT_DEFERRED_KHR");
            break;
        #endif
        #if defined(VK_EXT_pipeline_creation_cache_control)
        #if defined(VK_KHR_get_physical_device_properties2) || defined(VK_VERSION_1_1)
        case VK_PIPELINE_COMPILE_REQUIRED_EXT:
            sprintf(string, "VK_PIPELINE_COMPILE_REQUIRED_EXT");
            break;
        #endif
        #endif
        #if defined(VK_KHR_video_encode_queue)
        #if defined(VK_KHR_video_queue) &&  ( defined(VK_KHR_synchronization2) || defined(VK_VERSION_1_3) ) 
        case VK_ERROR_INVALID_VIDEO_STD_PARAMETERS_KHR:
            sprintf(string, "VK_ERROR_INVALID_VIDEO_STD_PARAMETERS_KHR");
            break;
        #endif
        #endif
        #if defined(VK_EXT_image_compression_control)
        #if defined(VK_KHR_get_physical_device_properties2) || defined(VK_VERSION_1_1)
        case VK_ERROR_COMPRESSION_EXHAUSTED_EXT:
            sprintf(string, "VK_ERROR_COMPRESSION_EXHAUSTED_EXT");
            break;
        #endif
        #endif
        #if defined(VK_EXT_shader_object)
        #if  (  ( defined(VK_KHR_get_physical_device_properties2) || defined(VK_VERSION_1_1) )  && defined(VK_KHR_dynamic_rendering) )  || defined(VK_VERSION_1_3)
        case VK_INCOMPATIBLE_SHADER_BINARY_EXT:
            sprintf(string, "VK_INCOMPATIBLE_SHADER_BINARY_EXT");
            break;
        #endif
        #endif
        default:
            sprintf(string, "Invalid VkResult value: %u", value);
            return 0;
    }
    return 1;
}

