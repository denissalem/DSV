/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vulkan/vulkan.h>

#ifndef DSV_IMAGES_H
#define DSV_IMAGES_H

typedef struct DSVImages_t {
  uint32_t buffer_count;
  VkImage * buffer;
  uint32_t * views_count;
  VkImageView  ** views;
} DSVImages;

VkImageViewCreateInfo DSV_init_VkImageViewCreateInfo();

#endif
