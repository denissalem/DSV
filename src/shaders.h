/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_SHADER_H
#define DSV_SHADER_H

typedef struct DSVShader_t {
    unsigned char * payload;
    uint32_t  size;
} DSVShader;

uint32_t DSV_create_fragment_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    const char * entry_point_name
);

uint32_t DSV_create_shader_module(
    VkDevice logical_device,
    DSVShader shader,
    VkShaderModule * shader_module
);

uint32_t DSV_create_shader_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    VkShaderStageFlagBits stage_flag_bits,
    const char * entry_point_name
);

uint32_t DSV_create_vertex_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    const char * entry_point_name
);

DSVShader DSV_load_shader(const char * filename);

uint32_t DSV_load_n_create_shader(
    VkDevice logical_device,
    const char * filename,
    VkShaderModule * shader_module
);
#endif
