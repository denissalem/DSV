/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "memory_management.h"

static void ** allocation_register;
static uint32_t allocation_register_index;
static uint32_t allocation_register_size;

uint32_t DSV_init_memory_management() {
    allocation_register = 0;
    allocation_register_index = 0;
    allocation_register_size = DSV_ALLOCATION_REGISTER_CHUNK_SIZE;
    allocation_register = malloc(sizeof(uint_fast64_t) * allocation_register_size);
    if (allocation_register == NULL) {
        
    }
    explicit_bzero(allocation_register, sizeof(uint_fast64_t) * allocation_register_size);
    return 1;
}

uint32_t DSV_free(void * buffer) {    
    if (buffer == 0) {
        return 1;
    }
    
    for (uint32_t i = 0; i < allocation_register_size; i++) {
        if(allocation_register[i] == (void *) buffer) {
            free((void *)allocation_register[i]);
            allocation_register[i] = 0;
            for (uint32_t j = i+1; j < allocation_register_size; j++) {
                if (allocation_register[j] == 0) {
                    break;
                }
                allocation_register[j-1] = allocation_register[j];
            }
            break;
        }
    }
    if (allocation_register_index < allocation_register_size - DSV_ALLOCATION_REGISTER_CHUNK_SIZE) {
        allocation_register = realloc(allocation_register, sizeof(void *) * (allocation_register_size - DSV_ALLOCATION_REGISTER_CHUNK_SIZE));
        if (allocation_register == NULL) {
            printf("DSV Memory Management: allocation register resizing failed.\n");
            return 0;
        }
        allocation_register_size -= DSV_ALLOCATION_REGISTER_CHUNK_SIZE;
    }
    allocation_register_index--;
    return 1;
}

void DSV_free_all() {
    for (uint32_t i = 0; i < allocation_register_index; i++) {
        free((void *)allocation_register[i]);
    }
    free(allocation_register);
    return;
}

void * DSV_malloc(uint32_t buffer_size) {
    if (allocation_register_index >= allocation_register_size) {
        allocation_register = realloc(allocation_register, sizeof(void *) * (allocation_register_size + DSV_ALLOCATION_REGISTER_CHUNK_SIZE));
        if (allocation_register == NULL) {
            printf("DSV Memory Management: allocation register resizing failed.\n");
            return NULL;
        }
        explicit_bzero(&allocation_register[allocation_register_size], sizeof(void *) * DSV_ALLOCATION_REGISTER_CHUNK_SIZE);
        allocation_register_size += DSV_ALLOCATION_REGISTER_CHUNK_SIZE;
    }
    allocation_register[allocation_register_index] = (void *) malloc(buffer_size);
    if (allocation_register[allocation_register_index] == 0) {
            printf("DSV Memory Management: allocation failed.\n");
            return NULL;
    }
    explicit_bzero((void *) allocation_register[allocation_register_index], buffer_size);
    return (void *) allocation_register[allocation_register_index++];
}
