/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

uint32_t DSV_create_graphic_pipeline(
    VkDevice logical_device,
    VkPipeline * pipeline,
    VkGraphicsPipelineCreateInfo * create_info
) {
    if (create_info->layout == VK_NULL_HANDLE) {
        #ifdef DEBUG
        fprintf(stderr, "\033[33mDSV: VkGraphicsPipelineCreateInfo has NULL VkPipelineLayout.\033[0m\n");
        #endif
        VkPipelineLayoutCreateInfo pipeline_layout_create_info = {0};
        pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        
        DSV_create_pipeline_layout(
            logical_device,
            &pipeline_layout_create_info,
            &create_info->layout
        );
    }
    
    DSV_trace_VkGraphicsPipelineCreateInfo(create_info);
    
    VkResult result = vkCreateGraphicsPipelines(logical_device, VK_NULL_HANDLE, 1, create_info, NULL, pipeline);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "\e[1;31mDSV: vkCreateGraphicsPipelines failed with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
        return result;
    }

    return result;
}

DSVGraphicPipelineCreateInfo DSV_get_graphic_pipeline_create_info(
    DSVPipelineComponentsUseFlag components_use_flag
) {
    DSVGraphicPipelineCreateInfo create_info_wrapper = {0};
    create_info_wrapper.create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    for (uint32_t bit = 0; bit < 32; bit++) {
        uint32_t mask = components_use_flag & (1 << bit);
        switch(mask) {
            case DSV_PIPELINE_NO_COMPONENTS:
                break;
            case DSV_PIPELINE_VERTEX_INPUT:
                create_info_wrapper.vertex_input_state = DSV_malloc(sizeof(VkPipelineVertexInputStateCreateInfo));
                create_info_wrapper.vertex_input_state->sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pVertexInputState = (const VkPipelineVertexInputStateCreateInfo*) create_info_wrapper.vertex_input_state;
                break;
            case DSV_PIPELINE_IMPUT_ASSEMBLY:
                create_info_wrapper.input_assembly_state = DSV_malloc(sizeof(VkPipelineInputAssemblyStateCreateInfo));
                create_info_wrapper.input_assembly_state->sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pInputAssemblyState = (const VkPipelineInputAssemblyStateCreateInfo*) create_info_wrapper.input_assembly_state;
                break;
            case DSV_PIPELINE_STENCIL_AND_DEPTH:
                create_info_wrapper.depth_stencil_state = DSV_malloc(sizeof(VkPipelineDepthStencilStateCreateInfo));
                create_info_wrapper.depth_stencil_state->sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pDepthStencilState = (const VkPipelineDepthStencilStateCreateInfo*) create_info_wrapper.depth_stencil_state;
                break;
            case DSV_PIPELINE_VIEWPORT_AND_SCISSOR:
                create_info_wrapper.viewport_state = DSV_malloc(sizeof(VkPipelineViewportStateCreateInfo));
                create_info_wrapper.viewport_state->sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pViewportState = (const VkPipelineViewportStateCreateInfo*) create_info_wrapper.viewport_state;
                break;
            case DSV_PIPELINE_RASTERIZATION:
                create_info_wrapper.rasterization_state = DSV_malloc(sizeof(VkPipelineRasterizationStateCreateInfo));
                create_info_wrapper.rasterization_state->sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pRasterizationState = (const VkPipelineRasterizationStateCreateInfo*) create_info_wrapper.rasterization_state;
                break;
            case DSV_PIPELINE_MULTISAMPLING:
                create_info_wrapper.multisample_state = DSV_malloc(sizeof(VkPipelineMultisampleStateCreateInfo));
                create_info_wrapper.multisample_state->sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pMultisampleState = (const VkPipelineMultisampleStateCreateInfo *) create_info_wrapper.multisample_state;
                break;
            case DSV_PIPELINE_COLOR_BLEND:
                create_info_wrapper.color_blend_state = DSV_malloc(sizeof(VkPipelineColorBlendStateCreateInfo));
                create_info_wrapper.color_blend_state->sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pColorBlendState = (const VkPipelineColorBlendStateCreateInfo *) create_info_wrapper.color_blend_state;
                break;
            case DSV_PIPELINE_DYNAMIC_STATE:
                create_info_wrapper.dynamic_state = DSV_malloc(sizeof(VkPipelineDynamicStateCreateInfo));
                create_info_wrapper.dynamic_state->sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pDynamicState = (const VkPipelineDynamicStateCreateInfo *) create_info_wrapper.dynamic_state;
                break;
            case DSV_PIPELINE_TESSELLATION_STATE:
                create_info_wrapper.tessellation_state = DSV_malloc(sizeof(VkPipelineTessellationStateCreateInfo));
                create_info_wrapper.tessellation_state->sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
                create_info_wrapper.create_info.pTessellationState = (const VkPipelineTessellationStateCreateInfo *) create_info_wrapper.tessellation_state;
                break;
            #ifdef DEBUG
            default:
                fprintf(stderr, "\e[1;31mDSV: Invalid DSVPipelineComponentsUseFlag: %u.\n\e[0m", mask);        
            #endif
        }
    } 

    create_info_wrapper.create_info.layout = VK_NULL_HANDLE;

    return create_info_wrapper;
}
