/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include <vulkan.h>
    
#ifndef DSV_DEBUG_H
#define DSV_DEBUG_H

#define DSV_TRACE_ERROR(message) \
    fprintf(stderr, "\e[1;31mDSV: %s\n\e[0m", message);

uint32_t DSV_VkResult_to_string(uint32_t value, char * debug);

#ifdef DEBUG
#define DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(function_name, result) \
    if (result != VK_SUCCESS) { \
        char function_name_ret_to_string[96] = {0};\
        uint32_t function_name_ret_to_string_is_valid = DSV_VkResult_to_string(result, function_name_ret_to_string);\
        fprintf(stderr, "\e[1;31mDSV: " #function_name " failed: %s\n\e[0m", function_name_ret_to_string); \
        return result; \
    }
#else
#define DSV_RETURN_AND_TRACE_IF_VK_FUNCTION_FAIL(function_name, result) \
    if (result != VK_SUCCESS) { \
        fprintf(stderr, "\e[1;31mDSV: " #function_name " failed: %u\n\e[0m", result); \
        return result; \
    }
#endif

    // TODO TRACE VkPhysicalDeviceProperties
    // TODO TRACE VkPhysicalDeviceFeatures

    //~ void DSV_print_flag_VkAccessFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkAttachmentDescriptionFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkColorComponentFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkCommandBufferUsageFlag(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkCommandPoolCreateFlag(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkCompositeAlphaFlagsKHR(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkDependencyFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkDeviceCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkFramebufferCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkImageAspectFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkImageUsageFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkImageViewCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineColorBlendStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineDepthStencilStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineDynamicStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineInputAssemblyStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineLayoutCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineMultisampleStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineRasterizationStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineShaderStageCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineStageFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineTessellationStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineVertexInputStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkPipelineViewportStateCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkQueryControlFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkQueryPipelineStatisticFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkRenderPassCreateFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkSampleCountFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkShaderStageFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkSubpassDescriptionFlags(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkSurfaceTransformFlagsKHR(uint32_t flag, const char * indent_level);
    //~ void DSV_print_flag_VkSwapchainCreateFlagsKHR(uint32_t flag, const char * indent_level);

    //~ void DSV_trace_VkCommandBufferBeginInfo(VkCommandBufferBeginInfo * begin_info);
    //~ void DSV_trace_VkCommandBufferAllocateInfo(VkCommandBufferAllocateInfo * alloc_info);
    //~ void DSV_trace_VkCommandPoolCreateInfo(VkCommandPoolCreateInfo * create_info);
    //~ void DSV_trace_VkDeviceCreateInfo(VkDeviceCreateInfo * create_info);
    //~ void DSV_trace_VkFramebufferCreateInfo(VkFramebufferCreateInfo * create_info);
    //~ void DSV_trace_VkImageViewCreateInfo(VkImageViewCreateInfo * create_info);
    //~ void DSV_trace_VkGraphicsPipelineCreateInfo(VkGraphicsPipelineCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineColorBlendAttachmentState(VkPipelineColorBlendAttachmentState * attachments);
    //~ void DSV_trace_VkPipelineColorBlendStateCreateInfo(VkPipelineColorBlendStateCreateInfo* create_info);
    //~ void DSV_trace_VkPipelineDepthStencilStateCreateInfo(VkPipelineDepthStencilStateCreateInfo* create_info);
    //~ void DSV_trace_VkPipelineDynamicStateCreateInfo(VkPipelineDynamicStateCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineInputAssemblyStateCreateInfo(VkPipelineInputAssemblyStateCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineLayoutCreateInfo(VkPipelineLayoutCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineMultisampleStateCreateInfo(VkPipelineMultisampleStateCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineRasterizationStateCreateInfo(VkPipelineRasterizationStateCreateInfo* create_info);
    //~ void DSV_trace_VkPipelineShaderStageCreateInfo(VkPipelineShaderStageCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineTessellationStateCreateInfo(VkPipelineTessellationStateCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineVertexInputStateCreateInfo(VkPipelineVertexInputStateCreateInfo * create_info);
    //~ void DSV_trace_VkPipelineViewportStateCreateInfo(VkPipelineViewportStateCreateInfo * create_info);
    //~ void DSV_trace_VkPresentInfoKHR(VkPresentInfoKHR * info);
    //~ void DSV_trace_VkRenderPassBeginInfo(VkRenderPassBeginInfo * begin_info);
    //~ void DSV_trace_VkRenderPassCreateInfo(VkRenderPassCreateInfo * create_info);
    //~ void DSV_trace_VkStencilOpState(VkStencilOpState * stencil_op_state);
    //~ void DSV_trace_VkSubmitInfo(VkSubmitInfo * info);
    //~ void DSV_trace_VkSurfaceCapabilitiesKHR(VkSurfaceCapabilitiesKHR * capabilities);
    //~ void DSV_trace_VkSwapchainCreateInfoKHR(VkSwapchainCreateInfoKHR * create_info);
    
    //~ // Not meant to be used by end-user
    //~ void _indent_DSV_trace_VkPipelineColorBlendAttachmentState(VkPipelineColorBlendAttachmentState * attachments, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineColorBlendStateCreateInfo(VkPipelineColorBlendStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineDepthStencilStateCreateInfo(VkPipelineDepthStencilStateCreateInfo* create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineDynamicStateCreateInfo(VkPipelineDynamicStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineInputAssemblyStateCreateInfo(VkPipelineInputAssemblyStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineMultisampleStateCreateInfo(VkPipelineMultisampleStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineRasterizationStateCreateInfo(VkPipelineRasterizationStateCreateInfo* create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineShaderStageCreateInfo(VkPipelineShaderStageCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineTessellationStateCreateInfo(VkPipelineTessellationStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineVertexInputStateCreateInfo(VkPipelineVertexInputStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkPipelineViewportStateCreateInfo(VkPipelineViewportStateCreateInfo * create_info, const char * indent);
    //~ void _indent_DSV_trace_VkStencilOpState(VkStencilOpState * stencil_op_state, const char * indent);

#endif

