/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_PIPELINES_H
#define  DSV_PIPELINES_H

#define DSV_RECYCLE 1
#define DSV_DO_NOT_RECYCLE 0

typedef struct DSVGraphicPipelineCreateInfo_t {
    VkGraphicsPipelineCreateInfo              create_info;
    VkPipelineShaderStageCreateInfo           * stages;
    VkPipelineVertexInputStateCreateInfo      * vertex_input_state;
    VkPipelineInputAssemblyStateCreateInfo    * input_assembly_state;
    VkPipelineTessellationStateCreateInfo     * tessellation_state;
    VkPipelineViewportStateCreateInfo         * viewport_state;
    VkPipelineRasterizationStateCreateInfo    * rasterization_state;
    VkPipelineMultisampleStateCreateInfo      * multisample_state;
    VkPipelineDepthStencilStateCreateInfo     * depth_stencil_state;
    VkPipelineColorBlendStateCreateInfo       * color_blend_state;
    VkPipelineDynamicStateCreateInfo          * dynamic_state;
} DSVGraphicPipelineCreateInfo;

typedef uint32_t DSVPipelineComponentsUseFlag;

typedef enum DSVPipelineComponentsUseFlagBit_t {
    DSV_PIPELINE_NO_COMPONENTS = 0,
    DSV_PIPELINE_VERTEX_INPUT = 1,
    DSV_PIPELINE_IMPUT_ASSEMBLY = 2,
    DSV_PIPELINE_STENCIL_AND_DEPTH = 4,
    DSV_PIPELINE_VIEWPORT_AND_SCISSOR = 8,
    DSV_PIPELINE_RASTERIZATION = 16,
    DSV_PIPELINE_MULTISAMPLING = 32,
    DSV_PIPELINE_COLOR_BLEND = 64,
    DSV_PIPELINE_DYNAMIC_STATE = 128,
    DSV_PIPELINE_TESSELLATION_STATE = 256,
} DSVPipelineComponentsUseFlagBit;

uint32_t DSV_create_graphic_pipeline(
    VkDevice logical_device,
    VkPipeline * pipeline,
    VkGraphicsPipelineCreateInfo * create_info
);

DSVGraphicPipelineCreateInfo DSV_get_graphic_pipeline_create_info(
    DSVPipelineComponentsUseFlag components_use_flag
);

#endif
