/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_SWAP_CHAIN_H
#define  DSV_SWAP_CHAIN_H

#include "framebuffers.h"

typedef struct DSVSwapchain_t {
    DSVSurface surface;
    VkSwapchainKHR swapchains;
    VkExtent2D * extent; 
    DSVFramebuffers framebuffers;
} DSVSwapchain;

uint32_t DSV_create_swap_chain(
    DSVDevice device,
    VkSurfaceKHR surface,
    uint32_t (*pick_swap_chain_surface_format_callback)(VkSurfaceFormatKHR * format, void * args),
    void * pick_swap_chain_surface_format_callback_args,
    VkSwapchainCreateInfoKHR * swap_chain_create_info,
    VkImageViewCreateInfo * image_view_create_info,
    DSVSwapchain swapchain
);

uint32_t DSV_swap_chain_extent_validation(
    const VkSurfaceCapabilitiesKHR capabilities
);

VkExtent2D DSV_get_surface_current_extent(
    VkPhysicalDevice device,
    VkSurfaceKHR surface
);

VkSurfaceFormatKHR DSV_get_swap_surface_format(
    VkPhysicalDevice physical_device,
    VkSurfaceKHR surface,
    uint32_t (*pick_swap_chain_surface_format_callback)(VkSurfaceFormatKHR * format, void * pick_swap_chain_surface_format_callback_args),
    void * pick_swap_chain_surface_format_callback_args
);

VkSwapchainCreateInfoKHR DSV_init_VkSwapchainCreateInfoKHR(VkSurfaceKHR surface);

#endif
