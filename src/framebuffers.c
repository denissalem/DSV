/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

VkResult DSV_create_framebuffers(DSVDevice device, DSVSwapchain swapchain, VkSurfaceKHR surface, VkRenderPass render_pass) {
    VkExtent2D extent = DSV_get_surface_current_extent(device.physical, surface);
    VkFramebufferCreateInfo framebuffer_create_info = {0};
    framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_create_info.renderPass = render_pass;
    framebuffer_create_info.attachmentCount = 1;
    framebuffer_create_info.width = extent.width;
    framebuffer_create_info.height = extent.height;
    framebuffer_create_info.layers = 1;

    swapchain.framebuffers.buffers = DSV_malloc(sizeof(VkFramebuffer) * framebuffers->count);

    VkResult result = VK_SUCCESS;
    for (uint32_t i = 0; i < swap_chain->count; i++) {
        framebuffer_create_info.pAttachments = &swap_chain->views[i];
        #ifdef DEBUG
        printf("%u: ", i); DSV_trace_VkFramebufferCreateInfo(&framebuffer_create_info);
        #endif
        result = vkCreateFramebuffer(device.logical, &framebuffer_create_info, NULL, &swap_chain->framebuffers[i]);
        if (result != VK_SUCCESS) {
            fprintf(stderr, "\e[1;31mDSV: Failed to create framebuffer with return code:\e[0m"); DSV_print_enum_VkResult(result, " ");
            return result;
        }
    }
    
    return result;
}
