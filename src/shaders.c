/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

uint32_t DSV_create_fragment_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    const char * entry_point_name
) {
    return DSV_create_shader_stage(
        create_info,
        logical_device,
        filename,
        VK_SHADER_STAGE_FRAGMENT_BIT,
        entry_point_name
    );
}

uint32_t DSV_create_shader_module(
    VkDevice logical_device,
    DSVShader shader,
    VkShaderModule * shader_module
) {
    VkShaderModuleCreateInfo create_info = {0};
    create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    create_info.codeSize = shader.size;
    create_info.pCode = (const uint32_t*) shader.payload;
    if (vkCreateShaderModule(logical_device, &create_info, 0, shader_module) != VK_SUCCESS) {
        printf("DSV: Failed to create shader module!\n");
        return 0;
    }
    return 1;
}

uint32_t DSV_create_shader_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    VkShaderStageFlagBits stage_flag_bits,
    const char * entry_point_name
) {
    
    VkShaderModule shader_module = {0};
    
    if (!DSV_load_n_create_shader(logical_device, filename, &shader_module)) {
        return 0;
    }
    
    create_info->sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    create_info->stage = stage_flag_bits;
    create_info->module = shader_module;
    create_info->pName = entry_point_name;
    
    #ifdef DEBUG
    DSV_trace_VkPipelineShaderStageCreateInfo(create_info);
    #endif
    
    return 1;
}

uint32_t DSV_create_vertex_stage(
    VkPipelineShaderStageCreateInfo * create_info,
    VkDevice logical_device,
    const char * filename,
    const char * entry_point_name
) {
    return DSV_create_shader_stage(
        create_info,
        logical_device,
        filename,
        VK_SHADER_STAGE_VERTEX_BIT,
        entry_point_name
    );
}

DSVShader DSV_load_shader(const char * filename) {
    FILE * shader_fp = fopen(filename, "r");
  
    DSVShader shader = {0};
            
    if (shader_fp) {
        fseek(shader_fp, 0L, SEEK_END);
        shader.size = ftell(shader_fp);
        shader.payload = DSV_malloc(sizeof(unsigned char)*shader.size);
        fseek(shader_fp, 0L, SEEK_SET);
        for(int i=0; i<shader.size; i++) {
            shader.payload[i] = (unsigned char) fgetc(shader_fp);
        }
        fclose(shader_fp);
    }
    else {
         printf("DSV: Failed to load %s\n", filename);
    }
    
    return shader;
}

uint32_t DSV_load_n_create_shader(
    VkDevice logical_device,
    const char * filename,
    VkShaderModule * shader_module
) {
    DSVShader shader = DSV_load_shader(filename);
    
    if (shader.payload == NULL) {
        return 0;
    }
    
    int ret =  DSV_create_shader_module(
        logical_device,
        shader,
        shader_module
    );
    DSV_free(shader.payload);
    return ret;
}
