/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"
#include "validation_layers.h"

#ifndef DSV_INSTANCES_H
#define DSV_INSTANCES_H

typedef struct DSVRequiredInstanceExtensions_t {
    uint32_t        count;
    const char **   names;
} DSVRequiredInstanceExtensions;

VkResult DSV_create_instance(
    VkInstance * instance,
    DSVRequiredInstanceExtensions * required_extensions,
    DSVRequiredValidationLayers * required_validation_layers,
    const char * application_name,
    uint32_t application_version,
    const char * engine_name,
    uint32_t engine_version,
    uint32_t vulkan_api_version
);

#endif
