/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdio.h"

#include "debug.h"
#include "render_pass.h"

uint32_t DSV_create_render_pass(
    VkDevice device,
    void (*write_create_info_callback)(VkRenderPassCreateInfo * create_info),
    VkRenderPass * render_pass
) {
    VkRenderPassCreateInfo create_info = {0};
    create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;

    write_create_info_callback(&create_info);

    #ifdef DEBUG
    DSV_trace_VkRenderPassCreateInfo(&create_info);
    #endif
    
    VkResult result = vkCreateRenderPass(device, &create_info, NULL, render_pass);

    if (result != VK_SUCCESS) {
        printf("DSV: Failed to create render pass!\n");
        return 0;
    }
    
    return 1;
}
