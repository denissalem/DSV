/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#ifndef DSV_QUEUES_H
#define DSV_QUEUES_H

void DSV_for_each_queue_familie(
    VkPhysicalDevice physical_device,
    int (*do_callback)(
        VkPhysicalDevice physical_device,
        uint32_t current_familiy_queue_index,
        VkQueueFamilyProperties * current_queue_family,
        void * do_callback_args
    ),
    void * do_callback_args
);

VkQueue DSV_get_device_queue(
    DSVDevice device,
    uint32_t queueFamilyIndex,
    uint32_t queueIndex,
    VkQueue* pQueue
);

#endif
