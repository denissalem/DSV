/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <string.h>

#include <vulkan/vulkan.h>

#include "memory_management.h"
#include "validation_layers.h"

VkResult DSV_check_validation_layer_support(
    DSVRequiredValidationLayers * required_validation_layers
) {
    uint32_t available_layer_count;
    VkLayerProperties * available_layers = 0;
  
    vkEnumerateInstanceLayerProperties(&available_layer_count, 0);
    available_layers = DSV_malloc(sizeof(VkLayerProperties)* available_layer_count);
    vkEnumerateInstanceLayerProperties(&available_layer_count, available_layers);

    for (int i = 0; i < required_validation_layers->count; i++) {
        int layer_found = 0;
        for (int j = 0; j < available_layer_count ; j++) {
            if (strcmp(required_validation_layers->names[i], available_layers[j].layerName) == 0) {
                layer_found = 1;
                break;
            }
        }
        if (!layer_found) {
            #ifdef DEBUG
            fprintf(stderr, "\e[1;31mDSV: Missing %s\n\n\e[0m", required_validation_layers->names[i]);
            #endif
            DSV_free(available_layers);
            return VK_ERROR_LAYER_NOT_PRESENT;
        }
    }
    
    DSV_free(available_layers);
    return VK_SUCCESS;
}
