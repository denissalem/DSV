/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

uint32_t choose_swap_surface_format(VkSurfaceFormatKHR * format, void * args);

void setup_swap_chain_requirement(
    VkSwapchainCreateInfoKHR * swap_chain_create_info, 
    VkImageViewCreateInfo * image_view_create_info,
    DSVDevice * device,
    VkSurfaceKHR surface
);
