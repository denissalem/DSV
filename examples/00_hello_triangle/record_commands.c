/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "record_commands.h"

uint32_t record_commands(VkCommandBuffer * command_buffer, void * args) {
    DSVBasicDrawingCommandsRequirements * requirements = (DSVBasicDrawingCommandsRequirements *) args;
    
    VkRenderPassBeginInfo render_pass_info = {0};
    render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_info.renderPass = *requirements->render_pass;
    render_pass_info.framebuffer = requirements->swap_chain->framebuffers[requirements->available_image_index]; 
    render_pass_info.renderArea.extent = requirements->swap_chain->extent;

    VkClearValue clear_color = {{{0.0f, 0.0f, 0.0f, 1.0f}}};
    render_pass_info.clearValueCount = 1;
    render_pass_info.pClearValues = &clear_color;
    DSV_trace_VkRenderPassBeginInfo(&render_pass_info);

    vkCmdBeginRenderPass(*command_buffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(*command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *requirements->graphic_pipeline);
    DSV_set_scissor_and_viewport(command_buffer, requirements->swap_chain->extent);
    vkCmdDraw(*command_buffer, 3, 1, 0, 0);
    
    vkCmdEndRenderPass(*command_buffer);
    
    return VK_SUCCESS;
}

