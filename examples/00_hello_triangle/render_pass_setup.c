/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "render_pass_setup.h"

VkAttachmentDescription attachment_description = {0};
VkSubpassDescription sub_passes = {0};
VkSubpassDependency dependencies = {0};
VkAttachmentReference attachment_reference = {0};

void setup_render_pass(VkRenderPassCreateInfo * create_info) {
  
    create_info->attachmentCount = 1;
    create_info->pAttachments = &attachment_description;
    
    attachment_description.format = VK_FORMAT_B8G8R8A8_SRGB;
    attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    sub_passes.pColorAttachments = &attachment_reference;
    sub_passes.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    sub_passes.colorAttachmentCount = 1;   
    create_info->subpassCount = 1;
    create_info->pSubpasses = &sub_passes;   

    dependencies.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies.dstSubpass = 0;
    dependencies.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies.srcAccessMask = 0;
    dependencies.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    create_info->dependencyCount = 1;
    create_info->pDependencies = &dependencies;     
    
    attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachment_reference.attachment = 0;
}
