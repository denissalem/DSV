/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

DSVDevice get_device(VkInstance instance, DSVSurface surface);

//~ uint32_t match_physical_device_n_setup_queue_family(
    //~ VkInstance * instance,
    //~ VkSurfaceKHR * surface,
    //~ VkPhysicalDevice physical_device,
    //~ VkDeviceCreateInfo * device_create_info,
    //~ void * args
//~ );

//~ typedef struct QueuesFamilyMatchArgs {
    //~ VkSurfaceKHR * surface;
    //~ uint32_t index;
    //~ uint32_t check;
//~ } QueuesFamilyMatchArgs;

//~ VkDeviceCreateInfo setup_device_requirement(
    //~ DSVRequiredDeviceExtensions * extensions,
    //~ DSVRequiredValidationLayers * validation_layers
//~ );

//~ extern QueuesFamilyMatchArgs g_graphique_queue_family;
