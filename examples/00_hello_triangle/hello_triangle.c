/*
 * Copyright 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DSV.h"

#include "device.h"
#include "instance.h"
#include "window.h"

//~ void main_loop(DSVDevice device,  DSVBasicDrawingCommandsRequirements * basic_drawing_commands_requirements) {
    //~ DSVSemaphores semaphores = DSV_get_semaphores(device, 1, 1);
    //~ semaphores.wait_stages[0] = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;    
    //~ VkFence fence =  DSV_get_fence(device, VK_FENCE_CREATE_SIGNALED_BIT);
        
    //~ /******************** COMMAND POOLS *********************/ 
    
    //~ VkCommandPool command_pool = {0};
    //~ VkCommandBuffer command_buffer = {0};
    
    //~ if(DSV_create_command_pool(
        //~ device.logical,
        //~ VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        //~ g_graphique_queue_family.index,
        //~ &command_pool
    //~ ) != VK_SUCCESS) {
        //~ DSV_free_all();
        //~ return;
    //~ };
    
    //~ if(DSV_create_command_buffer(
        //~ device.logical,
        //~ &command_pool,
        //~ &command_buffer,
        //~ VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        //~ 1
    //~ ) != VK_SUCCESS) {
        //~ DSV_free_all();
        //~ return;
    //~ };
        
    //~ // Retrieve Queue
    //~ VkQueue graphics_queue = {0};
    //~ vkGetDeviceQueue(device.logical, g_graphique_queue_family.index, 0, &graphics_queue);
        
    //~ while (!glfwWindowShouldClose(window)) {
        //~ glfwPollEvents();
        
        //~ vkWaitForFences(device.logical, 1, &fence, VK_TRUE, UINT64_MAX);
        //~ vkResetFences(device.logical, 1, &fence);
                       
        //~ if (DSV_acquire_next_image_KHR(
            //~ device.logical,
            //~ basic_drawing_commands_requirements->swap_chain->swap_chain,
            //~ UINT64_MAX,
            //~ semaphores.waits[0],
            //~ VK_NULL_HANDLE,
            //~ &basic_drawing_commands_requirements->available_image_index
        //~ ) != VK_SUCCESS) {
            //~ break;
        //~ }
                
        //~ if(DSV_record_commands(
            //~ &command_buffer,
            //~ 0, 
            //~ (void*) NULL,
            //~ record_commands,
            //~ basic_drawing_commands_requirements
        //~ ) != VK_SUCCESS) {
            //~ DSV_free_all();
            //~ break;
        //~ };
        
        //~ printf("TRACE THERE\n");
        //~ if (DSV_submit_commands(
            //~ &command_buffer,
            //~ 1,
            //~ semaphores,
            //~ fence,
            //~ graphics_queue
        //~ ) != VK_SUCCESS) {
            //~ DSV_free_all();
            //~ break;
        //~ }
        
        //~ if (DSV_queue_present(semaphores) != VK_SUCCESS) {
            //~ DSV_free_all();
            //~ break;
        //~ }
    //~ }
    
    //~ DSV_free_semaphores(device, semaphores);
    //~ vkDestroyFence(device.logical, fence, NULL);
//~ }

int main(int argc, char ** argv) {
    DSV_init_memory_management();
    init_window("Hello Triangle");
    
    VkInstance instance = create_instance();
    if(instance == VK_NULL_HANDLE) {
        return -1;
    }

    DSVSurface window_surface = {0};
    if (glfwCreateWindowSurface(instance, window, 0, &window_surface.surface) != VK_SUCCESS) {
        DSV_TRACE_ERROR("GLFW ERROR: Failed to create window surface!");
        DSV_free_all();
        return -1;
    }

    DSVDevice device = get_device(instance, window_surface);
    if (device.physical == VK_NULL_HANDLE || device.logical == VK_NULL_HANDLE) {
        return -1;
    }
    
    /******************** SWAP CHAIN  *********************/
    
    //~ DSVSwapchain swapchain = {0};
    //~ VkSwapchainCreateInfoKHR swap_chain_create_info = DSV_init_VkSwapchainCreateInfoKHR(surface);
    //~ VkImageViewCreateInfo image_view_create_info = DSV_init_VkImageViewCreateInfo();
    //~ setup_swap_chain_requirement(&swap_chain_create_info, &image_view_create_info, &device, surface);

    //~ uint32_t swap_chain_has_been_created = DSV_create_swap_chain(
        //~ &device,
        //~ surface,
        //~ choose_swap_surface_format,
        //~ NULL,
        //~ &swap_chain_create_info,
        //~ &image_view_create_info,
        //~ &swapchain
    //~ );
    
    //~ if(!swap_chain_has_been_created) {
        //~ DSV_free_all();
        //~ return -1;
    //~ }
    
    //~ /******************** RENDER PASS ***************************/
    
    //~ VkRenderPass render_pass = {0};
    
    //~ if(!DSV_create_render_pass(device.logical, setup_render_pass, &render_pass)) {
        //~ DSV_free_all();
        //~ return -1;
    //~ }
    
    //~ /******************** GRAPHIC PIPELINE  *********************/
    
    //~ DSVGraphicPipelineCreateInfo graphic_pipeline_ci = DSV_get_graphic_pipeline_create_info(
        //~ DSV_PIPELINE_VERTEX_INPUT |
        //~ DSV_PIPELINE_IMPUT_ASSEMBLY |
        //~ DSV_PIPELINE_VIEWPORT_AND_SCISSOR |
        //~ DSV_PIPELINE_RASTERIZATION |
        //~ DSV_PIPELINE_MULTISAMPLING |
        //~ DSV_PIPELINE_COLOR_BLEND | 
        //~ DSV_PIPELINE_DYNAMIC_STATE
    //~ );
    
    //~ uint32_t graphic_pipeline_has_been_created = setup_graphic_pipeline(
        //~ &graphic_pipeline_ci,
        //~ device,
        //~ surface,
        //~ render_pass
    //~ );
    
    //~ graphic_pipeline_has_been_created = DSV_create_graphic_pipeline(
        //~ device.logical,
        //~ &graphic_pipeline_ci.create_info.basePipelineHandle,
        //~ &graphic_pipeline_ci.create_info
    //~ );
    
    //~ if(graphic_pipeline_has_been_created != VK_SUCCESS) {
        //~ DSV_free_all();
        //~ return -1;
    //~ }
    
    //~ /******************** FRAMEBUFFERS *********************/
    
    //~ if (DSV_create_framebuffers(device, &swap_chain, surface, render_pass) != VK_SUCCESS) {
        //~ DSV_free_all();
        //~ return -1;
    //~ }
    
    
    //~ /**************** COMMANDS PRÉPARATION ******************/
    
    //~ DSVBasicDrawingCommandsRequirements basic_drawing_commands_requirements = {0};
    //~ basic_drawing_commands_requirements.render_pass = &render_pass;
    //~ basic_drawing_commands_requirements.swap_chain = &swap_chain;
    //~ basic_drawing_commands_requirements.graphic_pipeline = &graphic_pipeline_ci.create_info.basePipelineHandle;
    
    //~ /********************* MAIN LOOP ************************/
    
    //~ main_loop(device, &basic_drawing_commands_requirements);
    
    DSV_free_all();
    return 0;
}
