/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

//~ #include "queues.h"

#include "device.h"

DSVRequiredDeviceExtensions required_device_extensions = {
    1,
    {VK_KHR_SWAPCHAIN_EXTENSION_NAME}
};

VkPhysicalDevice device_picker(DSVPhysicalDevices physicals, void * args) {
    VkPhysicalDeviceProperties device_properties = {0};
    VkPhysicalDeviceFeatures deviceFeatures = {0};

    for (unsigned int i = 0; i < physicals.count; i++) {
        vkGetPhysicalDeviceProperties(physicals.devices[i], &device_properties);
        vkGetPhysicalDeviceFeatures(physicals.devices[i], &deviceFeatures);
    }
    
    return VK_NULL_HANDLE;
}

DSVDevice get_device(VkInstance instance, DSVSurface surface) {
    DSVDevice device = {0};
    
    VkDeviceCreateInfo device_create_info = DSV_allocate_VkDeviceCreateInfo(
        NULL,
        &required_device_extensions,
        1
    );
    
    float queue_family_priorities[] = {1.0f};
    VkDeviceQueueCreateInfo queue_create_info = {
        VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        0,
        0,
        0,
        1,
        queue_family_priorities
    };
    
    device_create_info.pQueueCreateInfos = &queue_create_info;

    DSV_pick_physical_device(
        instance,
        &device,
        device_picker,
        NULL
    );

    return device;

    //~ uint32_t device_has_been_acquired = DSV_get_device(
        //~ match_physical_device_n_setup_queue_family,
        //~ &instance,
        //~ &surface,
        //~ &device_create_info,
        //~ NULL,
        //~ &device
    //~ );
    
    //~ DSV_clear_VkDeviceCreateInfo(&device_create_info);

    //~ if (!device_has_been_acquired) {
        //~ DSV_free_all();
        //~ return -1;
    //~ }
}

//~ float queue_family_priorities = 1.0f;

//~ int match_device_queues_family_requirements(
    //~ VkPhysicalDevice physical_device,
    //~ uint32_t current_familiy_queue_index,
    //~ VkQueueFamilyProperties * current_queue_family,
    //~ void * do_callback_args
//~ ) {
    //~ QueuesFamilyMatchArgs * queues_family_match_args = (QueuesFamilyMatchArgs *) do_callback_args;
    //~ VkBool32 present_support = VK_FALSE;
    //~ vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, current_familiy_queue_index, *queues_family_match_args->surface, &present_support);
    
    //~ if ((current_queue_family->queueFlags & VK_QUEUE_GRAPHICS_BIT) && present_support) {
        //~ queues_family_match_args->index = current_familiy_queue_index;
        //~ queues_family_match_args->check = 1;
        //~ g_graphique_queue_family.check = 1;
        //~ g_graphique_queue_family.index = current_familiy_queue_index;
        //~ return 1;
    //~ }
    
    //~ queues_family_match_args->check = 0;
    //~ return 0;
//~ }

//~ uint32_t match_physical_device_n_setup_queue_family(
    //~ VkInstance * instance,
    //~ VkSurfaceKHR * surface,
    //~ VkPhysicalDevice physical_device,
    //~ VkDeviceCreateInfo * device_create_info,
    //~ void * args
//~ ) {
    //~ // Do We have the right queue family ?
    //~ QueuesFamilyMatchArgs queues_family_match = {surface, 0, 0};
    
    //~ DSV_for_each_queue_familie(
        //~ physical_device,
        //~ match_device_queues_family_requirements,
        //~ &queues_family_match
    //~ );
    
    //~ //Do we have required extensions ?
    //~ uint32_t extensions_supported = DSV_check_device_extension_support(
        //~ physical_device,
        //~ device_create_info
    //~ );
    
    //~ // Do we have Swap Chain support ?
    //~ DSVDeviceSwapChainSupportDetails swap_chain_support_details = {0};
    //~ uint32_t swap_chain_supported = 0;

    //~ if (extensions_supported) {
      //~ swap_chain_support_details = DSV_get_device_swap_chain_support(*surface, physical_device);
      //~ swap_chain_supported =\
          //~ swap_chain_support_details.format_count &&\
          //~ swap_chain_support_details.present_mode_count;
    //~ }

    //~ uint32_t match = queues_family_match.check && extensions_supported && swap_chain_supported;
    //~ DSV_clear_device_swap_chain_support(&swap_chain_support_details);

    //~ if (match) {
        //~ ((VkDeviceQueueCreateInfo*) device_create_info->pQueueCreateInfos)[0].queueFamilyIndex = queues_family_match.index;
        //~ return 1;
    //~ }
    
    //~ return 0;
//~ }
