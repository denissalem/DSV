/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "debug.h"
#include "shaders.h"

#include "graphic_pipeline_setup.h"

VkRect2D scissor = {0};
VkViewport viewport = {0};

VkDynamicState dynamic_states[3] = {
    VK_DYNAMIC_STATE_LINE_WIDTH,
    VK_DYNAMIC_STATE_SCISSOR,
    VK_DYNAMIC_STATE_VIEWPORT
};

VkPipelineColorBlendAttachmentState color_blend_attachment = {0};
VkPipelineShaderStageCreateInfo shader_stages[2] = {0};

uint32_t setup_graphic_pipeline(
    DSVGraphicPipelineCreateInfo * graphic_pipeline_ci,
    DSVDevice device,
    VkSurfaceKHR surface,
    VkRenderPass render_pass
) {
    /******** Render Pass *********/
    
    graphic_pipeline_ci->create_info.renderPass = render_pass;
  
    /********* Shader stags *********/
  
    if(!DSV_create_vertex_stage(&shader_stages[0], device.logical, "vert.spv", "main")) {
         printf("ERROR: failed to setup vertex shader stage\n");
         return 0;
    }
    
    if(!DSV_create_fragment_stage(&shader_stages[1], device.logical, "frag.spv", "main")) {
         printf("ERROR: failed to setup fragment shader stage\n");
         return 0;
    }

    /********* Graphic pipeline create infos *********/

    // Shader stages
    graphic_pipeline_ci->create_info.stageCount = 2;
    graphic_pipeline_ci->create_info.pStages = &shader_stages[0];

    // Vertex Input: None. Vertices are hard coded in shaders
    
    // Input Assembly :
    graphic_pipeline_ci->input_assembly_state->topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    
    // Viewport and Scissor
    VkSurfaceCapabilitiesKHR    capabilities = {0};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.physical, surface, &capabilities);
    
    viewport.width = (float) capabilities.currentExtent.width;
    viewport.height = (float) capabilities.currentExtent.height;
    viewport.maxDepth = 1.0f;
    
    scissor.extent = capabilities.currentExtent;

    graphic_pipeline_ci->viewport_state->viewportCount = 1;
    graphic_pipeline_ci->viewport_state->pViewports = &viewport;
    graphic_pipeline_ci->viewport_state->scissorCount = 1;
    graphic_pipeline_ci->viewport_state->pScissors = &scissor;

    // Rasterizer
    
    graphic_pipeline_ci->rasterization_state->polygonMode = VK_POLYGON_MODE_FILL;
    graphic_pipeline_ci->rasterization_state->lineWidth = 1.0f;
    graphic_pipeline_ci->rasterization_state->cullMode = VK_CULL_MODE_BACK_BIT;
    graphic_pipeline_ci->rasterization_state->frontFace = VK_FRONT_FACE_CLOCKWISE;
    
    // Multisampling
    graphic_pipeline_ci->multisample_state->rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    graphic_pipeline_ci->multisample_state->minSampleShading = 1.0f;

    // Color Blend

    color_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    color_blend_attachment.blendEnable = VK_FALSE;
    color_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optionnel
    color_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optionnel
    color_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD; // Optionnel
    color_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optionnel
    color_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optionnel
    color_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optionnel

    graphic_pipeline_ci->color_blend_state->logicOpEnable = VK_FALSE;
    graphic_pipeline_ci->color_blend_state->logicOp = VK_LOGIC_OP_COPY;
    graphic_pipeline_ci->color_blend_state->attachmentCount = 1;
    graphic_pipeline_ci->color_blend_state->pAttachments = &color_blend_attachment;
    graphic_pipeline_ci->color_blend_state->blendConstants[0] = 0.0f; // Optionnel
    graphic_pipeline_ci->color_blend_state->blendConstants[1] = 0.0f; // Optionnel
    graphic_pipeline_ci->color_blend_state->blendConstants[2] = 0.0f; // Optionnel
    graphic_pipeline_ci->color_blend_state->blendConstants[3] = 0.0f; // Optionnel
    
    // Dynamic states
    
    graphic_pipeline_ci->dynamic_state->dynamicStateCount = 3;
    graphic_pipeline_ci->dynamic_state->pDynamicStates = &dynamic_states[0];
    
    return 1;
}
