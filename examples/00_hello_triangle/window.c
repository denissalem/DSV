/*
 * Copyright 2023 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "window.h"

uint32_t window_width = 800;
uint32_t window_height = 600;

int framebuffer_resized = 0;

GLFWwindow * window = 0;

DSVRequiredInstanceExtensions required_glfw_extensions = {0};

static void framebuffer_resize_callback(GLFWwindow* window, int width, int height) {
    framebuffer_resized = 1;
    window_width = width;
    window_height = height;
}

void init_window(const char * window_name) {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    window = glfwCreateWindow(window_width, window_height, window_name, 0, 0);
    required_glfw_extensions.names = glfwGetRequiredInstanceExtensions(&required_glfw_extensions.count);
    #ifdef DEBUG
        printf("GLFW Required extensions:\n");
        for (int i = 0; i < required_glfw_extensions.count; i++) {
            printf("\t%s\n", required_glfw_extensions.names[i]);
        }
    #endif
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);
}
