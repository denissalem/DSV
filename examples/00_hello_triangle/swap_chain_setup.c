/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "swap_chain_setup.h"

uint32_t choose_swap_surface_format(VkSurfaceFormatKHR * format, void * args) {
    if (format->format == VK_FORMAT_B8G8R8A8_SRGB && format->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
        return 1;
    }
    return 0;
}

void setup_swap_chain_requirement(
    VkSwapchainCreateInfoKHR * swap_chain_create_info, 
    VkImageViewCreateInfo * image_view_create_info,
    DSVDevice * device,
    VkSurfaceKHR surface
) {
    VkSurfaceCapabilitiesKHR    capabilities = {0};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device->physical, surface, &capabilities);  
  
    swap_chain_create_info->presentMode = VK_PRESENT_MODE_FIFO_KHR;
    swap_chain_create_info->minImageCount = capabilities.minImageCount + 1;
    swap_chain_create_info->imageArrayLayers = 1;
    swap_chain_create_info->imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swap_chain_create_info->imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swap_chain_create_info->queueFamilyIndexCount = 0;
    
    swap_chain_create_info->pQueueFamilyIndices = 0;
    swap_chain_create_info->preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    swap_chain_create_info->compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swap_chain_create_info->clipped = VK_TRUE;
    swap_chain_create_info->oldSwapchain = VK_NULL_HANDLE;
    
    image_view_create_info->components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info->components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info->components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info->components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info->subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_create_info->subresourceRange.baseMipLevel = 0;
    image_view_create_info->subresourceRange.levelCount = 1;
    image_view_create_info->subresourceRange.baseArrayLayer = 0;
    image_view_create_info->subresourceRange.layerCount = 1;
}
