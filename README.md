<div align="center">
<img src="https://framagit.org/denissalem/DSV/-/raw/master/DSV_logo.png" title="DSV" alt="DSV">
</div>

Stand for __Damn Simple Vulkan__ It's a collection of Vulkan helpers to achieve well and quick what you have in mind. For now it's highly experimental and not documented.
Do not rely on it for production.

## Project structure

- __src__ : Actual source code of the library.
- __examples__ : Used both for demonstration, debuging and developpment.
- __vk_registry_processing__ : Collection of Python scripts for processing and generating C sources from [vk.xml](https://raw.githubusercontent.com/KhronosGroup/Vulkan-Docs/main/xml/vk.xml)

## WIP

- Many Vulkan functions return VkResult. Theses valus should be printed out in debug mode for clarity.
- Instead of return False or True as a result of DSV_do_something, return the Actual VkResult if any.

## TODO

### Queue management is a mess

The way queues are managed in example and DSV is __damn messy__ instead of _damn simple_ ...
   
### Memory management

Register all destructible Vulkan Object with VkAllocationCallbacks.

### Debuging

- Could use some macro in debug functions.
- In debug function, check for NULL pointer when it should not be in given Vulkan structure.
  
### Others

- Implement exceptions management.
- Complete all todo Comment in the code.
