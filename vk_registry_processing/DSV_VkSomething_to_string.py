#! /usr/bin/env python3

# Copyright 2023, 2024 Denis Salem
# 
# This file is part of DSV.
# 
# DSV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DSV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DSV. If not, see <http://www.gnu.org/licenses/>.

import re
import sys
import xml.etree.ElementTree as ET
from urllib.request import urlopen 

ALIAS = []

enum_C_function_header="""
/*
 * Copyright 2023, 2024 Denis Salem
 *
 * This file is part of DSV.
 *
 * DSV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DSV. If not, see <http://www.gnu.org/licenses/>.
*/

#include "../../debug.h"

uint32_t DSV_VK_ENUM_NAME_to_string(uint32_t value, char * string) {
    switch(value) {
"""
enum_C_function_footer="""        default:
            sprintf(string, "Invalid VK_ENUM_NAME value: %u", value);
            return 0;
    }
    return 1;
}
"""

def cherry_pick_element(registry, tag, name):
    for child in registry:
        if child.tag == tag and child.attrib["name"] == name:
            return child

def depends_to_C_format(depends):
    output = ""
    for token in depends.replace("+", " && ").replace(","," || ").replace("(","( ").replace(")"," )").split(" "):
        if len(token):
            if token.strip() in ["(",")","&&","||"]:
                output+= " "+token.strip()+" "
                
            else:
                output += "defined("+token.strip()+")"
            
    return output
    
def print_enum_extensions(registry, name):
        for child in registry:
            if child.tag == "extensions":
                for extension in child:
                    extension_deps = extension.attrib["depends"] if "depends" in extension.attrib.keys() else ""
                    for require in extension:
                        require_deps = require.attrib["depends"] if "depends" in require.attrib.keys() else ""
                        for require_child in require:
                            # Something wrong there
                            # See: https://registry.khronos.org/vulkan/specs/1.3/registry.html#_enum_tags
                            if require_child.tag == "enum" and "extends" in require_child.attrib.keys() and require_child.attrib["extends"] == name:
                                if ((not "alias" in require_child.attrib.keys() ) or (not require_child.attrib["alias"] in ALIAS)) and not require_child.attrib["name"] in ALIAS:
                                    print("        #if defined("+extension.attrib["name"]+")")
                                    if len(extension_deps):
                                        print("        #if", depends_to_C_format(extension_deps))
                                    if len(require_deps):
                                        print("        #if", depends_to_C_format(require_deps))
                                    
                                    print("        case", require_child.attrib["name"]+":")
                                    print("            sprintf(string, \""+require_child.attrib["name"]+"\");")
                                    print("            break;")
                                    
                                    if len(require_deps):
                                        print("        #endif")
                                    if len(extension_deps):
                                        print("        #endif")
                                    print("        #endif")
                                
                                ALIAS.append(require_child.attrib["name"])
                                if "alias" in require_child.attrib.keys():
                                    ALIAS.append(require_child.attrib["alias"])
                                    
def process_enums(element):    
    print(enum_C_function_header.replace("VK_ENUM_NAME", name).strip())
    
    for child in element:
        if child.tag == "enum":
            print("        case", child.attrib["name"]+":")
            print("            sprintf(string, \""+child.attrib["name"]+"\");")
            print("            break;")
            ALIAS.append(child.attrib["name"])
            
    print_enum_extensions(registry, name)            
    
    print(enum_C_function_footer.replace("VK_ENUM_NAME", name))
    
try:
    tag, name = sys.argv[1:]

except ValueError:
    print("usage: ./DSV_VkSomethig_to_string.py <tag> <name>")
    exit()

with urlopen('https://raw.githubusercontent.com/KhronosGroup/Vulkan-Docs/main/xml/vk.xml') as f:
    tree = ET.ElementTree(
      ET.fromstring(f.read().decode('utf-8'))
    )
    registry = tree.getroot()
    element = cherry_pick_element(tree.getroot(), tag, name)
    if element.attrib["type"] == "enum":
        process_enums(element)
